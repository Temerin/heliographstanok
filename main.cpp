#include "mainwindow.h"
#include "scene3d.h"
#include <QVector>
#include <QPoint>
#include <math.h>
#include "encoderport.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication::setOrganizationName( "Micran" );
    QApplication::setOrganizationDomain( "___________" );
    QApplication::setApplicationName( "StanokTest" );


    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    QObject::connect(&w, &MainWindow::showScene,[](Scene3D *scene) {
        scene->show();
    });

    return a.exec();
}
