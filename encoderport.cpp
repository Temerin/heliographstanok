#include "encoderport.h"
#include <QDebug>
#include "mathplus.h"

EncoderPort::EncoderPort()
{
    setTenzTable();
    connect(&thisPort, &QSerialPort::readyRead,this,&EncoderPort::ReadInPort);
}

qint32 EncoderPort::getKg()
{
    qint32 aver = (_tenz1 + _tenz2) /2;
    if(aver <= _tenzTable[0])
        return 0;
    for (int i = 0; i < _tenzTable.length() - 2; ++i) {
//        qDebug() << aver << _tenzTable[i];
        if (aver > _tenzTable[i]) {
            double diff = aver - _tenzTable[i];
            double step = _tenzTable[i+1] - _tenzTable[i];

            return double(i + (diff/step)) * 50;
        }
    }
    return 250000000;
}

qreal EncoderPort::getDeg2()
{
    qreal dev = 4096/0.84;
    dev = dev/5;
    qreal ab = 608.281;
    qreal ac = 608.595;
    qreal plusAngle = 27.872;
    qreal a1 = 90 - plusAngle;
    quint32 enc90d = 248081;
    qint32 delta = _encVec[1] - enc90d;
    qreal dbx = delta / dev;
    qreal abac2 = pow(ac,2) + pow(ab,2);
    qreal abac = 2*ab*ac;
    qreal bcOld = abac2 - (abac * cosd(a1));
    bcOld = sqrt(bcOld);
    qreal l = dbx + bcOld;
    qreal alpha = (abac2 - pow(l,2)) / abac;
    alpha = acosd(alpha);
    alpha = alpha + plusAngle - 90;
    return alpha;
}

qreal EncoderPort::getDeg34()
{
//    ang = 5;
//    h = 1194;
//    lh = 155;
//    ldh = 1468;
//    ldv = 585.23;

//    ldg = sqrt(ldh^2 + ldv^2)
//    lhg = sqrt(h^2 + lh^2)

//    ang1 = acosd(ldh/ldg)
//    ang2 = acosd(h/lhg)
//    ang_dr = 90 - ang1 - ang2

//    ldr = sqrt(ldg^2 + lhg^2 - 2*ldg*lhg*cosd(ang_dr))
//    len = sqrt(ldg^2 + lhg^2 - 2*ldg*lhg*cosd(ang_dr+ang))
//    ldr - len
    qreal dev = 7606/5;

//    qreal dev = 1521.2;
    qreal ab = 1204.0187;
    qreal ac = 1580.3538;
    qreal plusAngle = 29.131625;
    qreal a1 = 90 - plusAngle;
    quint32 enc90d = 16438657;
    qint32 delta = - _encVec[2] + enc90d;
    qreal dbx = delta / dev;
    qreal abac2 = pow(ac,2) + pow(ab,2);
    qreal abac = 2*ab*ac;
    qreal bcOld = abac2 - (abac * cosd(a1));
    bcOld = sqrt(bcOld);
    qreal l = dbx + bcOld;
    qreal alpha = (abac2 - pow(l,2)) / abac;
    alpha = acosd(alpha);
    alpha = alpha + plusAngle - 90;
    return alpha;
}

qreal EncoderPort::toMm(quint8 axis) //текущее значение
{
    if (axis == 2)
        return qreal(_encVec[1]*0.84); //*0.84 ???
    return qreal(_encVec[axis-1] / 1521.2);
}

qreal EncoderPort::toMm(quint8 axis, quint32 enc) // преобразование в соответствии с осью
{
    if (axis == 2)
        return qreal(enc); //*0.84 ???
    return qreal(enc / 1521.2);
}

qreal EncoderPort::toMmFromNull(quint8 axis) //текущее значение
{
    qreal mm = 0;
    qint64 delta = 0;
    qint64 test = 0;
    switch (axis) {
    case 1:
        test = _encVec[0]; //криво, косо, но там unsigned int, и это способ перевести;
        if (test < 150000) {
            delta = 86087 - test;
        } else {
            delta = 16777215 - test + 86087;
        }
        break;
    case 2:
        test = _encVec[1]; //криво, косо, но там unsigned int, и это способ перевести;
        delta = 248081 - test;
        mm = delta / -974.762;
        return mm;
        break;
    case 3:
        test = _encVec[2]; //криво, косо, но там unsigned int, и это способ перевести;
        delta = 16438657 - test;
        break;
    case 4:
        test = _encVec[3]; //криво, косо, но там unsigned int, и это способ перевести;
        delta = 16458211 - test;
        break;
    default:
        break;
    }
    mm = delta / 1521.2;
    return mm;
}

void EncoderPort::ReadInPort()
{
    QByteArray data;
    data.append(thisPort.readAll());
//    qDebug() << data.toHex();
    outPort(data);
    if(data[0] != char(0xAA) || data[1] != char(0x55) ||
            data[30] != char(0xBB) || data[31] != char(0x33))
        return;
    {
        uint32_t newEnc = 0;
        newEnc |= (quint8)data[2];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[3];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[4];

        if (_encVec[0] != newEnc) {
            _encVec[0] = newEnc;
//                emit encode(_enc);
            if (!_move1) {
                _counter1 = 0;
                _move1 = true;
//
                emit move1();
            }
        } else {
            if (_move1) {
                ++_counter1;
                if(_counter1 > 30){
                    _move1 = false;
//                    emit stop1();
                }
            }
        }
    }
    {
        uint32_t newEnc = 0;
        newEnc |= (quint8)data[5];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[6];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[7];
//        newEnc = newEnc << 8;

        if (_encVec[1] != newEnc) {
            _encVec[1] = newEnc;
//                emit encode(_enc);
            if (!_move2) {
                _counter2 = 0;
                _move2 = true;
//                emit move2();
            }
        } else {
            if (_move2) {
                ++_counter2;
                if(_counter2 > 30){
                    _move1 = false;
//                    emit stop2();
                }
            }
        }
    }
    {
        uint32_t newEnc = 0;
        newEnc |= (quint8)data[8];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[9];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[10];

        if (_encVec[2] != newEnc) {
            _encVec[2] = newEnc;
//                emit encode(_enc);
            if (!_move3) {
                _counter3 = 0;
                _move3 = true;
//                emit move3();
            }
        } else {
            if (_move3) {
                ++_counter3;
                if(_counter3 > 30){
                    _move3 = false;
//                    emit stop3();
                }
            }
        }
    }
    {
        uint32_t newEnc = 0;
        newEnc |= (quint8)data[11];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[12];
        newEnc = newEnc << 8;
        newEnc |= (quint8)data[13];

        if (_encVec[3] != newEnc) {
            _encVec[3] = newEnc;
//                emit encode(_enc);
            if (!_move4) {
                _counter4 = 0;
                _move4 = true;
//                emit move4();
            }
        } else {
            if (_move4) {
                ++_counter4;
                if(_counter4 > 30){
                    _move4 = false;
//                    emit stop4();
                }
            }
        }
    }
    {
        int newRev = 0;
        newRev |= (quint8)data[14];
        newRev = newRev << 8;
        newRev |= (quint8)data[15];
        if (_rev != newRev) {
            _rev = newRev;
            emit rev(_rev);
            if (!_toothCounter)
                _toothCounter = 1;
            emit tenzAver(_tenzToothCounter/_toothCounter, _maxTenz, _maxTooth, _minTenz, _minTooth);
            _toothCounter = 0;
            _tenzToothCounter = 0;
            _maxTenz = -2147483648;
            _maxTooth = 0;
            _minTenz = 2147483647;
            _minTooth = 0;
        }
    }
    {
        int newTenz1 = 0;
        newTenz1 |= (qint8)data[19];
        newTenz1 = newTenz1 << 8;
        newTenz1 |= (quint8)data[20];
        newTenz1 = newTenz1 << 8;
        newTenz1 |= (quint8)data[21];
        _tenz1 = newTenz1;
        emit tenz1(_tenz1);
        if (_tenzHardLimit > _tenz1)
            emit tenzHardLimit();
        if (_maxTenz < _tenz1) {
            _maxTenz = _tenz1;
            _maxTooth = _tooth % 244;
        }
        if (_minTenz > _tenz1) {
            _minTenz = _tenz1;
            _minTooth = _tooth % 244;
        }
    }
    {
        quint32 newTooth = 0;
        newTooth |= (quint8)data[16];
        newTooth = newTooth << 8;
        newTooth |= (quint8)data[17];
        newTooth = newTooth << 8;
        newTooth |= (quint8)data[18];
        if (_toothCounter == 0)
            _toothRev = newTooth;  //Чтобы считать 244 зуба
        if (_prevTooth != newTooth) {
            _tooth = newTooth;
            emit tooth(_tooth);
            _tenzToothCounter += _tenz1;
            ++_toothCounter;
            emit toothTenz(_tooth - _toothRev, _tenz1);
        }
        _prevTooth = newTooth;
    }
    bool newProbe = (quint8)data[25];
    if (_probe != newProbe) {
        _probe = newProbe;
        emit probe(_probe);
    }

}

void EncoderPort::setTenzTable()
{
    _tenzTable.append(80);
    _tenzTable.append(174);
    _tenzTable.append(259);
    _tenzTable.append(341);
    _tenzTable.append(422);
    _tenzTable.append(503);
    _tenzTable.append(590);
    _tenzTable.append(685);
    _tenzTable.append(770);
    _tenzTable.append(844);
    _tenzTable.append(940);
    _tenzTable.append(1020);
    _tenzTable.append(1100);
    _tenzTable.append(1180);
    _tenzTable.append(1273);
    _tenzTable.append(1365);
    _tenzTable.append(1435);
    _tenzTable.append(1520);
    _tenzTable.append(1609);
    _tenzTable.append(1694);
    _tenzTable.append(1779);
    _tenzTable.append(1863);
    _tenzTable.append(1948);
    _tenzTable.append(2033);
    _tenzTable.append(2117);
    _tenzTable.append(2202);
    _tenzTable.append(2287);
    _tenzTable.append(2371);
    _tenzTable.append(2456);
    _tenzTable.append(2541);
}


