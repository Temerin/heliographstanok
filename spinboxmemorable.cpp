#include "spinboxmemorable.h"
#include <QDebug>



SpinBoxMemorable::SpinBoxMemorable(QWidget *parent)
    : QSpinBox(parent)
{
    connect(this,&SpinBoxMemorable::editingFinished,this,&SpinBoxMemorable::setValueMem);
}

int SpinBoxMemorable::prevValue()
{
    return _prevValue;
}

void SpinBoxMemorable::setValueMem()
{
    if (_helper == value())
        return;
    _prevValue = _helper;
    _helper = value();
    emit editingFinishedWithSave();
}

void SpinBoxMemorable::setValueWithSave(int val)
{
    setValue(val);
    setValueMem();
}
