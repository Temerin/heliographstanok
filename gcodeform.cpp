#include "gcodeform.h"
#include <QDebug>
#include <mainwindow.h>
#include "math.h"
#include <qmath.h>

GCodeForm::GCodeForm()
{

}

GCodeForm::GCodeForm(/*QMap<QPoint,uint32_t> *calibMap, */quint8 mode=0,     //mode 0 = 1m, 1 = 1.8m, 2 = 3m;
                     double step=1, QObject *parent) : QObject(parent)
{
    auto angle = [mode](float x) {
        switch (mode) {
        case 0:
            return qRadiansToDegrees(qAtan((3200*x)/(250000-pow(x,2))));
            break;
        case 1:
            return qRadiansToDegrees(qAtan((4680*x)/(810000-pow(x,2))));
            break;
        case 2:
            return qRadiansToDegrees(qAtan((4480*x)/(2250000-pow(x,2))));
            break;
        default:
            return 0.0;
            break;
        }
    };
    _file.setFileName("./gcode.txt");
    if(!_file.open(QFile::WriteOnly | QFile::Text))
        qDebug() << "Ne otkrivayetsa file na zapis";
    qreal y = 50;
    for (qreal x = 0; y > 0; x+= step) {
        switch (mode) {
        case 0:
            y = (250000-pow(x,2))/(3200);
            break;
        case 1:
            y = (810000-pow(x,2))/(4680);
            break;
        case 2:
            y = (2250000-pow(x,2))/(4480);
            break;
        default:
            break;
        }
        for (int z = 1; z < 245; ++z) {
            _writeStream << "G1 X" << z
                     << " Y" << x
                     << " Z" << y/* + calibMap->value(QPoint(z+1,int(x)),0)*/
                     << " S" << angle(x)
                     << " F10"
                     << "\n";
        }
        _writeStream << "G54 G0 X244"
                 << " Y0"
                 << " Z0"
                 << "\n";
    }
    _writeStream.flush();
}

void GCodeForm::getNext(qreal x)
{
    qreal hp = 1325.5;//1428.6;
    qreal y = ((810000-pow(x,2))/(4680))*pow(1.006,((x*-1)/900)) + hp;
    auto angle = [](float x) {
        return qRadiansToDegrees(qAtan(-x/2340));
    };
//    if (x <= xMax) {
//        y = (250000-pow(xMax,2))/(3200) + hp;
//        emit parable(x,y,angle(xMax));
//        return;
//    }
    if (x == 0) {
        emit parable(x,y,0);
        return;
    }
    emit parable(x,y,angle(x));
    qDebug() << "X" << x << "Y" << y << "A" << angle(x);
//    emit angleY(angle(x), x);
}

qreal GCodeForm::getYCoordinate(qreal x)
{
    qreal hp = 1344.2;//1428.6;
    qreal y = (250000-pow(x,2))/(3200) + hp;
    return y;
}

qreal GCodeForm::getY(qreal x)
{
    if ((x < -900) || (x > 900))
        return 0;
    else
        return (810000-pow(x,2))/(4680);
}

//double GCodeForm::getCompensation(double pan) //По X - среднее. По Y - ближайшее. Пока так
//{
//    auto keys = _tiltCompensation->keys();
//    if (_tiltCompensation->value(pan))
//        return _tiltCompensation->value(pan);
//    auto rightKeyIt = std::find_if(keys.begin(), keys.end(), [pan](auto key) {
//        return key > pan;
//    });
//    auto deltaTilt = _tiltCompensation->value(*rightKeyIt) -
//                     _tiltCompensation->value(*(rightKeyIt-1));
//    auto deltaPan  = *rightKeyIt - *(rightKeyIt-1);
//    qDebug() << "ПОПРАВКА: " << (deltaTilt/deltaPan + _tiltCompensation->value(*(rightKeyIt-1)))      //Формула: ((y2-y1)/(x2-x1)+y1)*(newX-x1)
//                * (pan - *(rightKeyIt-1));
//    return (deltaTilt/deltaPan + _tiltCompensation->value(*(rightKeyIt-1)))      //Формула: ((y2-y1)/(x2-x1)+y1)*(newX-x1)
//            * (pan - *(rightKeyIt-1));
//}
