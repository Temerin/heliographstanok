#ifndef SCENE3D_H
#define SCENE3D_H

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QtOpenGL>
#include <QVector>
#include <QPoint>
#include "tenzgradient.h"

struct ToothColor
{
    GLfloat y;      // высотная координата точки. Лучше так для отрисовки кривых, хоть и данных больше
    QRgb color;
};

class Scene3D : public QOpenGLWidget
{

    public:
        Scene3D(QWidget* parent = 0);

        ~Scene3D();

        void clearToothMatrix();
        void colorTableRefresh(const QString &filePath);
//        void colorTableRefresh(const QString &filePath);

        void printCyrcleTenz(QPoint xy, QMap<int, int> toothTenz);

//       int hardLimit = -3000;
//       int upTarget  = -700;
//       int lowTarget = -500;
        int tickness = 5;
        bool paintMatrix = true;

    protected:
        void initializeGL();
        void resizeGL(int nWidth, int nHeight);
        void paintGL();
        void mousePressEvent(QMouseEvent* pe);
        void mouseMoveEvent(QMouseEvent* pe);
        void mouseReleaseEvent(QMouseEvent* pe);
        void wheelEvent(QWheelEvent* pe);
        void keyPressEvent(QKeyEvent* pe);

    private:
        void scale_plus();
        void scale_minus();
        void rotate_up();
        void rotate_down();
        void rotate_left();
        void rotate_right();
        void translate_up();
        void translate_down();
        void translate_left();
        void translate_right();
        void defaultScene();

        void drawAxis();

        void getVertexArray();
        void getColorArray();
        void getIndexArray();
        void drawFigure();
        void drawMatrix();


        GLfloat _xRot;
        GLfloat _yRot;
        GLfloat _zRot;
        GLfloat _zTra;
        GLfloat _yTra;
        GLfloat _nSca;

        QPoint _ptrMousePosition;

        TenzGradient _tGrad;
        QMap<int,QMap<int,ToothColor>> _xToothColor; // массив зубъев/давлений(цвет) по x
        QMap<int,QMap<int,ToothColor>> _xToothColorMatrix; // массив зубъев/давлений(цвет) по x (Голая сетка)
};

#endif // SCENE3D_H
