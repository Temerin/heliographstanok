#include "tenzgradient.h"
#include <QColor>
#include "mathplus.h"
#include <QDebug>



TenzGradient::TenzGradient()
{

}

void TenzGradient::addColor(int tenz, QRgb color)
{
    _tenzColorMap.insert(tenz, color);
}

QRgb TenzGradient::getColor(int tenz)
{
    if (_tenzColorMap.contains(tenz))
        return _tenzColorMap.value(tenz);
    if (tenz < _tenzColorMap.keys().first())
        return _tenzColorMap.values().first();
    if (tenz > _tenzColorMap.keys().last())
        return _tenzColorMap.values().last();

    auto setRGBRange = [](int intensity) {
        if (intensity < 0)
            return 0;
        else if (intensity > 255)
            return 255;
        return intensity;
    };

    for (int i = 0; i < _tenzColorMap.keys().count()-1; i++) {
        if ((_tenzColorMap.keys()[i] < tenz) &&
                (_tenzColorMap.keys()[i+1])> tenz) {
            QRgb newRgb = 0;
            QRgb rgb1 = _tenzColorMap.value(_tenzColorMap.keys()[i]);
            QRgb rgb2 = _tenzColorMap.value(_tenzColorMap.keys()[i+1]);
            //red
            int fa = (rgb1 >> 16) & 0xff;
            int fb = (rgb2 >> 16) & 0xff;
            int intensity = linearInterpolation(_tenzColorMap.keys()[i], fa,
                                                _tenzColorMap.keys()[i+1], fb,
                                                tenz);
            newRgb |= (setRGBRange(intensity) << 16);

            //green
            fa = (rgb1 >> 8) & 0xff;
            fb = (rgb2 >> 8) & 0xff;
            intensity = linearInterpolation(_tenzColorMap.keys()[i], fa,
                                                _tenzColorMap.keys()[i+1], fb,
                                                tenz);
            newRgb |= (setRGBRange(intensity) << 8);

            //blue
            fa = (rgb1) & 0xff;
            fb = (rgb2) & 0xff;
            intensity = linearInterpolation(_tenzColorMap.keys()[i], fa,
                                                _tenzColorMap.keys()[i+1], fb,
                                                tenz);
            newRgb |= setRGBRange(intensity);
            return newRgb;
        }
    }
    return 0;
}

void TenzGradient::clearColors()
{
    _tenzColorMap.clear();
}
