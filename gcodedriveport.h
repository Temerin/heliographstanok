#ifndef GCODEDRIVEPORT_H
#define GCODEDRIVEPORT_H

#include "driverport.h"

class GCodeDrivePort : public DriverPort
{
public:
    GCodeDrivePort();



public slots:

    void setCoordinate(qreal x, qreal y, qreal angle, qreal upFrom);
    void setXCoordinate(qreal x);




    quint8 WriteToPort(QByteArray data);
    void ReadInPort();
};

#endif // GCODEDRIVEPORT_H
