#ifndef LOGWRITER_H
#define LOGWRITER_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QTime>

class LogWriter : public QObject
{
    Q_OBJECT
public:
    explicit LogWriter(QObject *parent = nullptr);
    void writeLog(const QString &str);

signals:

public slots:

protected:
    QFile _file;
    QTextStream _writeStream{&_file};
    QTime _time;
    quint16 _counter;
};

#endif // LOGWRITER_H
