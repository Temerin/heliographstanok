#ifndef GCODEFORM_H
#define GCODEFORM_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QPoint>

class GCodeForm : public QObject
{
    Q_OBJECT
public:
    GCodeForm();
    GCodeForm(quint8 mode, double step, QObject *parent = nullptr);

    void getNext(qreal x);

signals:
    void angleY(qreal angle, qreal x);
    void parable(qreal x, qreal y, qreal angle);

public slots:


    qreal getYCoordinate(qreal x);
    qreal getY(qreal x);
private:
    QFile _file;
    QTextStream _writeStream{&_file};
    qreal _xPrev = 0;
    qreal _yPrev = 0;
    QPointF _pPrev;
};

#endif // GCODEFORM_H
