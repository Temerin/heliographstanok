#include <QtGui>
#include <math.h>
#include "scene3D.h"
#include "mathplus.h"


const static float pi=3.141593, k=pi/180;
GLfloat Scale = 0.01f;

GLfloat VertexArray[12][3]; // декларируем массив вершин
GLfloat ColorArray[12][3];  // декларируем массив цветов вершин
//GLint   TenzArray[244];  // массив зубъев/давлений
GLubyte IndexArray[20][3];  // декларируем массив индексов вершин

// конструктор класса Scene3D
Scene3D::Scene3D(QWidget* parent) : QOpenGLWidget(parent)
{
    colorTableRefresh("./TenzColor.ini");

    // setFormat(QGLFormat(QGL::DepthBuffer)); // использовать буфер глубины
                                               // установлено по умолчанию в контексте (Нет)

    // начальные значения данных-членов класса
    defaultScene();

    // передает дальше указатель на объект parent
}

Scene3D::~Scene3D()
{
    close();
}

void Scene3D::printCyrcleTenz(QPoint xy, QMap<int, int> toothTenz)
{
    _xToothColor.remove(xy.x());

    ToothColor tc;
    QMap<int,ToothColor> toothColor; // массив зубъев/давлений(цвет)
    foreach (const auto &key, toothTenz.keys()) {
        tc.y = xy.y() * Scale;
        tc.color = _tGrad.getColor(toothTenz.value(key)); //
        toothColor.insert(key,tc);
    }
    _xToothColor.insert(xy.x(),toothColor);
}

void Scene3D::initializeGL() // инициализация
{
    // цвет для очистки буфера изображения - будет просто фон окна
//    qglClearColor(Qt::white);
    glClearColor(1.0f, 1.0f, 1.0f, 1);
    glEnable(GL_DEPTH_TEST);  // устанавливает режим проверки глубины пикселей
//    glShadeModel(GL_FLAT);    // отключает режим сглаживания цветов
    // устанавливаем режим, когда строятся только внешние поверхности
    glEnable(GL_CULL_FACE);

//    getVertexArray(); // определить массив вершин
//    getColorArray();  // определить массив цветов вершин
//    getIndexArray();  // определить массив индексов вершин

    glEnableClientState(GL_VERTEX_ARRAY); // активизация массива вершин
    glEnableClientState(GL_COLOR_ARRAY);  // активизация массива цветов вершин
}

void Scene3D::resizeGL(int nWidth, int nHeight) // окно виджета
{
    glMatrixMode(GL_PROJECTION); // устанавливает текущей проекционную матрицу
    glLoadIdentity();            // присваивает проекционной матрице единичную матрицу

    // отношение высоты окна виджета к его ширине
    GLfloat ratio=(GLfloat)nHeight/(GLfloat)nWidth;

    // мировое окно
    if (nWidth>=nHeight)
       // параметры видимости ортогональной проекции
       glOrtho(-1.0/ratio, 1.0/ratio, -1.0, 1.0, -10.0, 1.0);
    else
       glOrtho(-1.0, 1.0, -1.0*ratio, 1.0*ratio, -10.0, 1.0);
    // плоскости отсечения (левая, правая, верхняя, нижняя, передняя, задняя)

    // параметры видимости перспективной проекции
    // glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 10.0);
    // плоскости отсечения (левая, правая, верхняя, нижняя, ближняя, дальняя)

    // поле просмотра
    glViewport(0, 0, (GLint)nWidth, (GLint)nHeight);
}

void Scene3D::paintGL() // рисование
{
    // glClear(GL_COLOR_BUFFER_BIT); // окно виджета очищается текущим цветом очистки
    // очистка буфера изображения и глубины
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // устанавливаем положение и ориентацию матрице моделирования
    glMatrixMode(GL_MODELVIEW);
   // загружаем единичную матрицу моделирования
    glLoadIdentity();

    // последовательные преобразования
    glScalef(_nSca, _nSca, _nSca);        // масштабирование
    glTranslatef(_yTra, _zTra, 0.0f);    // трансляция
    glRotatef(_xRot, 1.0f, 0.0f, 0.0f); // поворот вокруг оси X
    glRotatef(_yRot, 0.0f, 1.0f, 0.0f); // поворот вокруг оси Y
    glRotatef(_zRot, 0.0f, 0.0f, 1.0f); // поворот вокруг оси Z

    drawAxis();   // рисование осей координат
    drawFigure(); // нарисовать фигуру
    if (paintMatrix)
        drawMatrix(); // нарисовать сетку матрицы
}

void Scene3D::mousePressEvent(QMouseEvent* pe)
{
    // при нажатии пользователем кнопки мыши переменной ptrMousePosition
    // будет присвоена координата указателя мыши
    _ptrMousePosition = pe->pos();

    // ptrMousePosition = (*pe).pos(); // можно и так написать
}

void Scene3D::mouseReleaseEvent(QMouseEvent* pe) // отжатие клавиши мыши
{
    Q_UNUSED(pe)
}

// изменение положения стрелки мыши
void Scene3D::mouseMoveEvent(QMouseEvent* pe)
{
    // вычисление углов поворота
    _xRot += 180/_nSca*(GLfloat)(pe->y()-_ptrMousePosition.y())/height();
    _zRot += 180/_nSca*(GLfloat)(pe->x()-_ptrMousePosition.x())/width();

    _ptrMousePosition = pe->pos();

//    updateGL(); // обновление изображения
    update();
}

void Scene3D::wheelEvent(QWheelEvent* pe)
{
   if ((pe->delta())>0) scale_plus();
   else if ((pe->delta())<0) scale_minus();

   update();
}

void Scene3D::keyPressEvent(QKeyEvent* pe)
{
    switch (pe->key())
    {
    case Qt::Key_Plus:
        scale_plus();     // приблизить сцену
    break;

    case Qt::Key_R:
        scale_plus();     // приблизить сцену
    break;

    case Qt::Key_Equal:
        scale_plus();     // приблизить сцену
    break;

    case Qt::Key_Minus:
        scale_minus();    // удалиться от сцены
    break;

    case Qt::Key_F:
        scale_minus();    // удалиться от сцены
    break;

    case Qt::Key_Up:
        rotate_up();      // повернуть сцену вверх
    break;

    case Qt::Key_Z:
        rotate_up();      // повернуть сцену вверх
    break;

    case Qt::Key_Down:
        rotate_down();    // повернуть сцену вниз
    break;

    case Qt::Key_X:
        rotate_down();    // повернуть сцену вниз
    break;

    case Qt::Key_Left:
        rotate_left();     // повернуть сцену влево
    break;

    case Qt::Key_Q:
        rotate_left();     // повернуть сцену влево
    break;

    case Qt::Key_Right:
        rotate_right();   // повернуть сцену вправо
    break;

    case Qt::Key_E:
        rotate_right();   // повернуть сцену вправо
    break;

    case Qt::Key_W:
        translate_down(); // транслировать сцену вниз
    break;

    case Qt::Key_S:
        translate_up();   // транслировать сцену вверх
    break;

    case Qt::Key_A:
        translate_right(); // транслировать сцену вправа
    break;

    case Qt::Key_D:
       translate_left(); // транслировать сцену влево
    break;

    case Qt::Key_Space:  // клавиша пробела
        defaultScene();   // возвращение значений по умолчанию
    break;

    case Qt::Key_Escape: // клавиша "эскейп"
        this->close();    // завершает приложение
    break;
    }
    update();
}

void Scene3D::scale_plus()
{
   _nSca = _nSca*1.1;
}

void Scene3D::scale_minus()
{
   _nSca = _nSca/1.1;
}

void Scene3D::rotate_up()
{
   _xRot += 1.0;
}

void Scene3D::rotate_down()
{
   _xRot -= 1.0;
}

void Scene3D::rotate_left()
{
   _zRot += 1.0;
}

void Scene3D::rotate_right()
{
   _zRot -= 1.0;
}

void Scene3D::translate_up()
{
   _zTra += 0.05;
}

void Scene3D::translate_down()
{
    _zTra -= 0.05;
}

void Scene3D::translate_left()
{
    _yTra -= 0.05;
}

void Scene3D::translate_right()
{
    _yTra += 0.05;
}

void Scene3D::defaultScene()
{
   _xRot=-45; _yRot=0; _zRot=0; _yTra=0; _zTra=0; _nSca=0.1;
}

void Scene3D::drawAxis() // построить оси координат
{
    // устанавливаем ширину линии приближенно в пикселях
    glLineWidth(3.0f);
    // до вызова команды ширина равна 1 пикселю по умолчанию

    // устанавливаем цвет последующих примитивов
    glColor4f(1.00f, 0.00f, 0.00f, 1.0f);
    // ось x красного цвета
    glBegin(GL_LINE_LOOP); // построение линии
        glVertex3f( 1.0f,  0.0f,  0.0f); // первая точка
        glVertex3f(-1.0f,  0.0f,  0.0f); // вторая точка
        glEnd();

    glBegin(GL_LINES);
        glColor4f(0.00f, 0.50f, 0.00f, 1.0f);
        // ось y зеленого цвета
        glVertex3f( 0.0f,  1.0f,  0.0f);
        glVertex3f( 0.0f, -1.0f,  0.0f);

        glColor4f(0.00f, 0.00f, 1.00f, 1.0f);
        // ось z синего цвета
        glVertex3f( 0.0f,  0.0f,  1.0f);
        glVertex3f( 0.0f,  0.0f, -1.0f);
    glEnd();
}

void Scene3D::drawFigure() // построить фигуру
{
    qint32 toothOnTurn = 244;
    float angleOnTooth = 2*pi/toothOnTurn;
    float angle = 0;
    // устанавливаем ширину линии приближенно в пикселях
    glLineWidth(tickness);
    // до вызова команды ширина равна 1 пикселю по умолчанию

    float r = 0;

    auto glColorRgb = [](QRgb color) {
        glColor3ub(qRed(color),
                  qGreen(color),
                  qBlue(color));
    };

    foreach (const auto x, _xToothColor.keys()) {
        auto toothColorMap = _xToothColor.value(x);
        r = x * Scale;
        glBegin(GL_LINE_LOOP); // построение линии
        foreach (const int &tooth, toothColorMap.keys()) {
            // устанавливаем цвет последующих примитивов
            ToothColor toothColor = toothColorMap.value(tooth);
            glColorRgb(toothColor.color);
            angle = tooth * angleOnTooth;
            glVertex3f(GLfloat(r*cos(angle)),  GLfloat(r*sin(angle)), toothColor.y);
        }
        glEnd();
    }
    GLfloat y = 0.0f;
    glLineWidth(3.0f);
    glBegin(GL_LINE_STRIP); // построение линии
    glColor4f(0.5f,0.5f,0.5f,0.7f);
    for (int x=30; x<= 1000; x+=15) {
        r = x * Scale;
        if ((x < -900) || (x > 900))
            y = 0;
        else
            y = (810000-pow(x,2))/(4680);
        glVertex3f(GLfloat(r*cos(0)),  GLfloat(r*sin(0)), y*Scale);
    }
    glEnd();
}

void Scene3D::drawMatrix()
{
    qint32 toothOnTurn = 244;
    float angleOnTooth = 2*pi/toothOnTurn;
    float angle = 0;
    // устанавливаем ширину линии приближенно в пикселях
    glLineWidth(3);

    float r = 0;
    qreal y = 0;
    glColor4f(0.9f,0.9f,0.9f,0.9f);
    for (int x=30; x<= 960; x+=15) {
        r = x * Scale;
        glBegin(GL_LINE_LOOP); // построение линии
        for (int i=1; i<= 244; ++i) {
            // устанавливаем цвет последующих примитивов
            angle = i * angleOnTooth;
            if ((x < -900) || (x > 900))
                y = 0;
            else
                y = (810000-pow(x,2))/(4680);
            glVertex3f(GLfloat(r*cos(angle)),  GLfloat(r*sin(angle)), y*Scale);
        }
        glEnd();
    }
}

void Scene3D::clearToothMatrix()
{
    _xToothColor.clear();
    update();
}

void Scene3D::colorTableRefresh(const QString &filePath)
{
    _tGrad.clearColors();

    QFile file(filePath);
    if (file.open((QFile::ReadOnly | QFile::Text))) {
        QTextStream in(&file);
        //Сюда бы reserve от количества строк в файле
        int tenz  = 0;
        quint8 r = 0;
        quint8 g = 0;
        quint8 b = 0;
        auto deleteSpace = [](QString &line) { //Удаляет подстроку + пробелы после неё
            line.remove(0,line.indexOf(' ')+1);
            while((line[0] == ' ')&&(!line.isNull())) {
                line.remove(0,1);
            }
        };
        while (!in.atEnd()) {
            QString line = in.readLine();

            tenz = line.left(line.indexOf(' ')).toInt();
            deleteSpace(line);

            r = line.left(line.indexOf(' ')).toUShort();
            deleteSpace(line);
            g = line.left(line.indexOf(' ')).toUShort();
            deleteSpace(line);
            b = line.left(line.indexOf(' ')).toUShort();

            _tGrad.addColor(tenz,QColor(r, g, b).rgb());
        }
    } else {
        qDebug() << "Файл настройки цветов не открылся";
    }
}
