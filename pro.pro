#-------------------------------------------------
#
# Project created by QtCreator 2013-08-19T22:59:57
#
#-------------------------------------------------

QT      += core gui opengl
QT      += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++14
LIBS   += -lopengl32
LIBS   += -lglu32
LIBS   += -lglut32

TARGET   = my_shag
TEMPLATE = app


SOURCES += main.cpp\
    gcodedriveport.cpp \
    gcodeform.cpp \
    mainwindow.cpp \
    port.cpp \
    crc.c \
    driverport.cpp \
    encoderport.cpp \
    roleport.cpp \
    logwriter.cpp \
    scene3d.cpp \
    toothtenzwriter.cpp \
    tenzgradient.cpp \
    colorbutton.cpp \
    spinboxmemorable.cpp

HEADERS += mainwindow.h \
    gcodedriveport.h \
    gcodeform.h \
    port.h \
    crc.h \
    driverport.h \
    encoderport.h \
    roleport.h \
    logwriter.h \
    mathplus.h \
    scene3d.h \
    toothtenzwriter.h \
    tenzgradient.h \
    colorbutton.h \
    spinboxmemorable.h

FORMS   += mainwindow.ui

RESOURCES += \
    res.qrc


