#include "gcodedriveport.h"
#include "QDebug"

GCodeDrivePort::GCodeDrivePort()
{
    connect(&thisPort, &QSerialPort::readyRead,this,&GCodeDrivePort::ReadInPort);
}

void GCodeDrivePort::setCoordinate(qreal x, qreal y, qreal angle, qreal upFrom)
{
    Q_UNUSED(angle)
    Q_UNUSED(upFrom)
    QByteArray data;
    auto addMk = [this,&data](qint32 mkm) {
        data.append(char(mkm>>24));
        data.append(char(mkm>>16));
        data.append(char(mkm>>8));
        data.append(char(mkm));
    };
    data.append('G');
    data.append(' ');
    data.append('X');
    qint32 xmk = x*1000;
    addMk(xmk);
    data.append(' ');
    data.append('Y');
    qint32 ymk = y*1000;
    addMk(ymk);
    WriteToPort(data);
}

quint8 GCodeDrivePort::WriteToPort(QByteArray data)
{
    _move = true;
    if (thisPort.isOpen()) {
//        qDebug() << thisPort.portName() << ": " << data.toHex();
        thisPort.write(data);
    }
    inPort(data);
    return _numCom;
}

void GCodeDrivePort::ReadInPort()
{
    QByteArray data;
    data.append(thisPort.readAll());
//    emit driveStop(data[0]);
//    qDebug()  << thisPort.portName() << data.toHex();
    outPort(data);
}
