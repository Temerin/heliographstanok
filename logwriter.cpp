#include "logwriter.h"
#include <QDateTime>
#include <QDebug>

LogWriter::LogWriter(QObject *parent) : QObject(parent)
{
    QDateTime dataTime = QDateTime::currentDateTimeUtc();
    _file.setFileName("./log" +
                     QString::number(dataTime.date().day()) + "_" +
                     QString::number(dataTime.date().month()) + "_" +
                     QString::number(dataTime.date().year()) + "_" +
                     QString::number(dataTime.time().hour()) + "_" +
                     QString::number(dataTime.time().minute()) + ".txt");
    if(!_file.open(QFile::WriteOnly | QFile::Text))
        qDebug() << "Ne otkrivayetsa file logov";

    _time.start();
}

void LogWriter::writeLog(const QString &str)
{
    _writeStream << QTime::currentTime().toString("hh:mm:ss.zzz") << " " << str << "\n";
    ++_counter;
    if(_counter > 300) {
        _writeStream.flush();
        _counter = 0;
    }
}
