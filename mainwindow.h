#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "port.h"
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <memory>
#include <QVector>
#include <QVector2D>
#include <encoderport.h>
#include <QDateTime>
#include "gcodeform.h"
#include "scene3d.h"
#include "toothtenzwriter.h"

namespace Ui {
class MainWindow;
}

inline bool operator < (const QPoint &p1, const QPoint &p2)
{
    return p1.x() < p2.x();//  qFuzzyIsNull(p1.xp - p2.xp) && qFuzzyIsNull(p1.yp - p2.yp);
}

enum RoleMod
{
    kray = 0,
    centerP = 1
};

class DriverPort;
//class EncoderPort;
class RolePort;
class QTimer;
class LogWriter;
class ColorButton;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

    void loadTenzColorTible(const QString &filePath);
    void addColor(int tenz, QColor color);
    void comparePosition();
    void updatePosDriveMm();

    RolePort    *_rolePort{nullptr};
    DriverPort  *_driverPort{nullptr};
    EncoderPort *_encoderPort{nullptr};
    Scene3D *_scene1{nullptr};

public slots:
    void parseCalib(QFile &file);
    void waitStop(int axis);
    void waitAns(quint8 ans);
    bool roleStop();
    bool allObjInit();

    void createScene();

signals:
    void stopAll();

    void setCoordinate(qreal x, qreal y, qreal angle, qreal upFrom);
    void setCoordinateA(qreal x, qreal y, qreal angle, qreal upFrom);
    void setXCoordinate(qreal x);
    void moveOnProbe(qint32 speed);
    void move12(qint32 s1=0, qint32 s2=0, qint32 s3=0, qint32 s4=0);
    void setStep12(qint32 s1, qint32 s2, qint32 s3, qint32 s4);

    void role(int step,int speed,quint8 mode);
    void clear();
    void move(int step,int speed,quint8 mode);
    void moveDrive(quint8 mode, quint8 foward, quint32 step);

    void showScene(Scene3D *scene);
    void newSetX(qreal x);
    void cleanMatrix();
    void cleanColor();

protected slots:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_Btn_Search_clicked();
    void print(QString data);
    void enterRole();

    void logWrite(QPoint point, uint32_t enc);

    void on_Btn_Gcode_clicked();

    void on_pbRightD1_clicked();

    void on_pbLeftD1_clicked();

    void on_pbLeftD2_clicked();

    void on_pbRightD2_clicked();

    void on_pbLeftD3_clicked();

    void on_pbRightD3_clicked();

    void on_pbLeftD4_clicked();

    void on_pbRightD4_clicked();

    void on_pbLeftD34_clicked();

    void on_pbRightD34_clicked();

    void on_pbStop_clicked();

//    void on_pbFindNull_clicked();

    void on_Btn_Gcode_Next_clicked();

    void on_pbSet_3_clicked();

    void on_Btn_Gcode_Next_On_clicked(bool checked);

    void on_pbSetCoordinate_clicked();

    void on_BtnConnect_clicked();

    void on_BtnMeter_clicked(bool checked);

    void on_pbSet_4_clicked();

    void on_pbSetStep_clicked();

    void on_pbClear_clicked();

    void on_BtnMeter_2_clicked(bool checked);

    void on_pbSetSteps_clicked();

    void on_BtnMeter_3_clicked(bool checked);

    void on_comboBox_currentIndexChanged(int index);

    void on_pbUpD1_clicked();

    void on_pbDownD1_clicked();
    void on_pbSaveSettings_clicked();

    void on_pbLoadSettings_clicked();

    void on_pbFindNull_clicked();
    void on_pbSaveSettings_2_clicked();

    void on_pbLoadSettings_2_clicked();

    void on_checkBox_clicked(bool checked);

    void on_sbTenzHardLimit_editingFinished();

    void on_pbHorizont_clicked();

    void on_bpStartRole_clicked();

    void on_pbStartRoleB_clicked();

    void on_pbStopRole_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_pbClearScene_clicked();

    void on_pbClearScene_2_clicked();

    void on_tabWidget_2_currentChanged(int index);

//    void on_sbTenz_editingFinished();

//    void on_sbTenz_2_editingFinished();

    void on_pbClearScene_3_clicked();

    void on_chbPaint_clicked(bool checked);

    void returnToNull();

    void on_dsbSpeedRoll_valueChanged(double arg1);

    void on_spinBox_valueChanged(int arg1);

    void on_chbMatrix_clicked(bool checked);

    void on_pbOpenColor_clicked();

    void on_pbSaveColor_clicked();

    void on_pbReturnColor_clicked();

    void on_pbAddColor_clicked();

    void on_pbToTo_clicked();

    void on_pbToNull3_clicked();

    void accurateToNull();

    void on_chbPaintNewFile_clicked(bool checked);

    void on_BtnReturn_clicked();

    void on_pbToNull_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    std::unique_ptr<QMap<QPoint,uint32_t>> _calibData;
    QTimer *_timerPaint{nullptr};
    bool _start = false;
    bool _forceStop = false;
    QSettings _settings {"./settings.ini", QSettings::Format::IniFormat};
    int _speed = 150;
    quint8 _mode = 2;
    GCodeForm *_gCode{nullptr};
    bool moveUp = false;
    bool _meter = false;
    bool _findNull = false;
    quint32 _toothNumberNull = 0;
    bool _startCatalka = false;
    bool _pause = false;
    int _counter = 0;
    QFile _fileCalib;
    QTextStream _writeStream{&_fileCalib};
    qreal _x = -192;
    int _oldX = -192;
    quint32 _startTooth = 0;
    QVector<QVector<quint32>> e1Vec;
    QVector<QVector<quint32>> e2Vec;
    QVector<QVector<quint32>> e3Vec;
    QVector<QVector<quint32>> e4Vec;
    QVector<QVector<quint16>> revVec;
    QVector<QVector<quint32>> toothVec;
    QMap<int,int> _tenzToothMap;

    qint32 _offset = 0;
    qint32 _offsetStep = 0;

    qreal _angle2 = 90;
    qreal _angle34 = 90;

    QDateTime _timeStart;
    QTime _timeLeft;
    QTime _deltaTime;
    QTime _timeLineMeter;
    quint32 _lineCounter = 0;



    qreal _angle = 0;
    qreal _y = 0;
    bool _first = true;
    bool _pryam = false;

    quint8 _revCounter = 0;
    quint32 _toothCounter = 0;

    RoleMod _roleMode = RoleMod::kray;

    qreal _startX = 0;

    LogWriter *_logWriter;
    ToothTenzWriter *_ttWriter;

    QString _tenzColorFilePath = "./TenzColor.ini";
    QMap<int,QColor> _tenzColorMap;



    void wait(int msecs);
};

#endif // MAINWINDOW_H
