#include "driverport.h"
#include <QDebug>
#include "mathplus.h"


void DriverPort::move(int step, int speed, qint8 mode)
{
    Q_UNUSED(speed)
    QByteArray data;
    if(mode == 0) {
        stop();
        return;
    }
    data.append(char(0x03));
    data.append(char(mode % 2));
    data.append(char(step>>24));
    data.append(char(step>>16));
    data.append(char(step>>8));
    data.append(char(step));
    WriteToPort(data);
}

void DriverPort::moveDrive(quint8 mode, quint8 foward, quint32 step)
{
//    stop();
    QByteArray data;
    data.append(char(mode));
    data.append(char(foward));
    data.append(char(step>>24));
    data.append(char(step>>16));
    data.append(char(step>>8));
    data.append(char(step));
    WriteToPort(data);
}

void DriverPort::stop()
{
    QByteArray data;
    data.append(char(0x01));
    WriteToPort(data);
    _move = false;
}

qint32 DriverPort::getSignStepFromAngle2(qreal angle)
{
    qreal angleNull = 90 - 27.872;
    angle -= 27.872;
    qreal ab = 608.281;
    qreal ac = 608.595;

    qreal abac = qPow(ab,2) + qPow(ac,2);
    qreal cos2 = 2*ab*ac*qCos(qDegreesToRadians(angleNull));
    qreal bcOld = abac - cos2;
    bcOld = qSqrt(bcOld);
    cos2 = 2*ab*ac*qCos(qDegreesToRadians(angle));
    qreal bcNew = abac - cos2;
    bcNew = qSqrt(bcNew);
    qreal dif = bcNew-bcOld;
    qint32 step = dif*476.19;

//    qreal dy = ac - ac*qCos(qDegreesToRadians(angle-angleNull));
//    qreal dx = ac * qSin(qDegreesToRadians(angle-angleNull));

    return step;
}


qint32 DriverPort::getSignStepFromAngle34(qreal angle)
{
    qreal angleNull = 90 - 29.095;
    angle -= 29.095;
    qreal ab = 1209.969;
    qreal ac = 1580.3538;

    qreal abac = qPow(ab,2) + qPow(ac,2);
    qreal cos2 = 2*ab*ac*qCos(qDegreesToRadians(angleNull));
    qreal bcOld = abac - cos2;
    bcOld = qSqrt(bcOld);
    cos2 = 2*ab*ac*qCos(qDegreesToRadians(angle));
    qreal bcNew = abac - cos2;
    bcNew = qSqrt(bcNew);
    qreal dif = bcNew-bcOld;
    qint32 step = dif*400;

//    qreal dy = ac - ac*qCos(qDegreesToRadians(angle-angleNull));
//    qreal dx = ac * qSin(qDegreesToRadians(angle-angleNull));

    return step;
}

qint32 DriverPort::getSignStepFromAngle2New(qreal angle)
{
    qreal angleNull = 90 - 27.872;
    angle = angleNull - angle;
    qreal ab = 608.281;
    qreal ac = 608.595;

    qreal abac = qPow(ab,2) + qPow(ac,2);
    qreal cos2 = 2*ab*ac*qCos(qDegreesToRadians(angleNull));
    qreal bcOld = abac - cos2;
    bcOld = qSqrt(bcOld);
    cos2 = 2*ab*ac*qCos(qDegreesToRadians(angle));
    qreal bcNew = abac - cos2;
    bcNew = qSqrt(bcNew);
    qreal dif = bcNew-bcOld;
    qint32 step = dif*-476.19;

//    qreal dy = ac - ac*qCos(qDegreesToRadians(angle-angleNull));
//    qreal dx = ac * qSin(qDegreesToRadians(angle-angleNull));

    return step;
}


qint32 DriverPort::getSignStepFromAngle34New(qreal angle)
{
    qreal angleNull = 90 - 29.095;
    angle = angleNull - angle;
    qreal ab = 1209.969;
    qreal ac = 1580.3538;

    qreal abac = qPow(ab,2) + qPow(ac,2);
    qreal cos2 = 2*ab*ac*qCos(qDegreesToRadians(angleNull));
    qreal bcOld = abac - cos2;
    bcOld = qSqrt(bcOld);
    cos2 = 2*ab*ac*qCos(qDegreesToRadians(angle));
    qreal bcNew = abac - cos2;
    bcNew = qSqrt(bcNew);
    qreal dif = bcNew-bcOld;
    qint32 step = dif*-400;

//    qreal dy = ac - ac*qCos(qDegreesToRadians(angle-angleNull));
//    qreal dx = ac * qSin(qDegreesToRadians(angle-angleNull));

    return step;
}

void DriverPort::sendBack()
{
    auto inv = [this](quint32 index) {
        if(!_prevCom[index])
            _prevCom[index] = char(0x01);
        else
            _prevCom[index] = char(0x00);
    };
    inv(3);
    inv(8);
    inv(13);
    WriteToPort(_prevCom);
}

void DriverPort::setNull()
{
    for(int i=0; i<_stepVec.length(); ++i) {
        _stepVec[i] = 0;
    }
}

quint8 DriverPort::WriteToPort(QByteArray data)
{
    if(thisPort.isOpen()) {
        _move = true;
        data.push_front(0x55);
        data.push_front(0xAA);
        while (data.length() < 30) {
            data.append(char(0x00));
        }
        data.push_back(0xBB);
        data.push_back(0x33);
//        if(data[2]==char(0x12))
        data[23] = char(_numCom++);
//        qDebug() << thisPort.portName() << ": " << data.toHex();
        thisPort.write(data);
    }
    if(_numCom == 0)
        _numCom = 1;
    inPort(data);
    return _numCom;
//    _prevCom = data;
}

DriverPort::DriverPort()
{
    connect(&thisPort, &QSerialPort::readyRead,this,&DriverPort::ReadInPort);
    //    connect(&thisPort, &QSerialPort::readyRead,this,&DriverPort::ReadInPort);
}

qint32 DriverPort::toStep(quint8 axis, qreal mm)
{
    if (axis == 1)
        return static_cast<qint32>((mm * -400) + 0.5f);
    if (axis == 2)
        return getStepsD2(mm);
    else
        return static_cast<qint32>((mm * 400) + 0.5f);
}

qreal DriverPort::toMm(quint8 axis, qint32 step)
{
    qreal mm;
    if (axis == 2)
        mm = step / 476.19f;
    else
        mm = step / 400.0f;
    return mm;
}

void DriverPort::setX34(qreal x)
{
    qreal h = 2075.4516;
//    qDebug() << "setX" << x;
    qreal arcSin = qAsin(x/h);
//    qDebug() << "arcSin" << arcSin;
    arcSin = qRadiansToDegrees(arcSin) + 90;
//    qDebug() << "angle" << arcSin;
    qint32 newSteps34 = getSignStepFromAngle34(arcSin);
    move12(0,0,newSteps34-_stepVec[2],newSteps34-_stepVec[3]);
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setX(qreal x)
{
    qreal h = 2065.5;
    qreal k = -85;
    qreal angdef = atand(k/h);
//    qreal lenshdef = 170+67+110;
//    qreal upFrom = 5;
//    qint32 newSteps1;
    qint32 newSteps2;
    qint32 newSteps34;

    QPointF pos = getPosition(x,0,0.1);

//    qreal b2 = 118;
//    qreal A = b2 * sind(angle);
//    qreal B = b2 * cosd(angle);
//    //удлинение шпинделя
//    qreal xsh = pos.x() - B;
//    qreal ysh = pos.y() - A;
//    qreal lensh = sqrt(pow(xsh-x,2)+pow(ysh-y,2)) - lenshdef;
//    lensh = lensh - upFrom;
//    newSteps1 = getStepsD1(lensh);

    qreal angmv = atand(pos.x()/pos.y()) - angdef;
    qreal ramaLen = lenghtRama(angmv);
    newSteps34 = getStepsD34(ramaLen);
    qreal d2Len = lenghtVal(angmv);
    newSteps2 = getStepsD2(d2Len);

    move12(/*newSteps1 - _stepVec[0]*/0,
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
//    _stepVec[0]=newSteps1;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setXABolt(qreal x, qreal angle)
{
    qreal h = 2075.4516;
    qreal arcSin = qAsin(x/h);
    arcSin = qRadiansToDegrees(arcSin) + 90;
    qint32 newSteps34 = getSignStepFromAngle34(arcSin);
    qint32 newSteps2  = getSignStepFromAngle2(arcSin+angle-90);
    move12(0,
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setXA(qreal x, qreal angle)
{
    qreal d2 = 118;
//    qreal dx = d2 - d2 * qCos(qDegreesToRadians(angle));
    qreal dy = d2 * qSin(qDegreesToRadians(angle));
    qreal h = 2075.4516 - dy;
    qreal arcSin = qAsin(x/h);
    arcSin = qRadiansToDegrees(arcSin) + 90;
    qint32 newSteps34 = getSignStepFromAngle34(arcSin);
    qint32 newSteps2  = getSignStepFromAngle2(arcSin+angle);
    move12(0,
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setXASaveY(qreal x, qreal angle) //не работает
{
    qreal d2 = 118;
//    qreal dx = d2 - d2 * qCos(qDegreesToRadians(angle));
//    qreal dy = d2 * qSin(qDegreesToRadians(angle));
    qreal h = 2065.5;
    qreal k = 85;
    qreal angle3 = asind((x+d2-d2*cosd(angle))/qSqrt(pow(h,2)+pow(k,2)));

    qint32 newSteps34 = getSignStepFromAngle34New(angle3);
    qint32 newSteps2  = getSignStepFromAngle2New(angle3+angle);
//    qreal dy2 = h-qreal(h*qSin(qDegreesToRadians(angle3)));
//    y = y / qCos(qDegreesToRadians(angle));
//    qint32 newSteps1 = y*400;

    move12(- _stepVec[0],
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
    _stepVec[0]=0;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}


void DriverPort::setXYASaveY(qreal x, qreal y, qreal angle)
{
    qreal d2 = 118;
//    qreal dx = d2 - d2 * qCos(qDegreesToRadians(angle));
    qreal dy = d2 * qSin(qDegreesToRadians(angle));
    qreal h = 2075.4516 - dy;
    qreal arcSin = qRadiansToDegrees(qAsin(x/h)) + 90;
    qreal dy2 = h-h*qSin(qDegreesToRadians(arcSin));
    qint32 newSteps34 = getSignStepFromAngle34(arcSin);
    qint32 newSteps2  = getSignStepFromAngle2(arcSin+angle);
    qint32 newSteps1 = (-dy+dy2+y)*400/qCos(qDegreesToRadians(angle));

    move12(newSteps1 - _stepVec[0],
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
    _stepVec[0]=newSteps1;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}



void DriverPort::setA(qreal angle34, qreal angle2)
{
    angle34 = angle34 + 90;
    qint32 newSteps34 = getSignStepFromAngle34(angle34);
    qint32 newSteps2  = getSignStepFromAngle2(angle34+angle2);
    move12(0,
           newSteps2 - _stepVec[1],
           newSteps34 - _stepVec[2],
           newSteps34 - _stepVec[3]);
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setCoordinate(qreal x, qreal y, qreal angle, qreal upFrom) //Сюда засунуть всю логику Руслана
{
    bool moveDev = false;
    if (upFrom >= 5000) {
        moveDev = true;
        upFrom -= 5000;
    }
    angle = 0;
    qDebug() << "setCoordinate:" << x << y << angle;
//   qreal upZbort = 3.4;
    qreal hp = 1326.1;//1428.6;
    qreal h = 2045;
    qreal k = -85;
    qreal angdef = atand(k/h);
    qreal lenshdef = 170+67+110;
//    qreal upFrom = 5;
    qreal b2 = 118;
    qint32 newSteps1;
    qint32 newSteps2;
    qint32 newSteps34;

    QPointF posRole = getPosRole(x, hp, hp+upZbort, upFrom);
//    QPoint angRole = getAngleRole(x);
    QPointF pos = getPos3(posRole.x(),posRole.y(),angle,h);
//    QPointF pos = getPosition(x,y,angle);


    qreal A = b2 * sind(angle);
//    qreal B = b2 * cosd(angle);
    //удлинение шпинделя
//    qreal xsh = pos.x() - B;
    qreal ysh = pos.y() - A;
    qreal lensh = ysh - posRole.y() - lenshdef;
    newSteps1 = getStepsD1(lensh);

    qreal angmv = atand(pos.x()/pos.y()) - angdef;
    qreal ramaLen = lenghtRama(angmv);
    newSteps34 = getStepsD34(ramaLen);
    qreal d2Len = lenghtVal(angle + angmv);
    newSteps2 = getStepsD2(d2Len);

    qDebug() << "Steps: (1)" << newSteps1
             << "(2)" << newSteps2
             << "(34)" << newSteps34;
    if(moveDev && _first) {
        move12D(newSteps1 - _stepVec[0],
                newSteps2 - _stepVec[1],
                newSteps34 - _stepVec[2],
                newSteps34 - _stepVec[3]);
        _first = false;
    }
    else {
        move12(newSteps1 - _stepVec[0],
                newSteps2 - _stepVec[1],
                newSteps34 - _stepVec[2],
                newSteps34 - _stepVec[3]);
    }
    _stepVec[0]=newSteps1;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}

void DriverPort::setCoordinateA(qreal x, qreal y, qreal angle, qreal upFrom) //Сюда засунуть всю логику Руслана
{
    bool moveDev = false;
    if (upFrom >= 5000) {
        moveDev = true;
        upFrom -= 5000;
    }
    qDebug() << "setCoordinate:" << x << y << angle;
    qreal h = 2045;
    qreal k = -85;
    qreal angdef = atand(k/h);
    qreal lenshdef = 170+67+110;
    qreal b2 = 118;
    qint32 newSteps1;
    qint32 newSteps2;
    qint32 newSteps34;

    QPointF pos = getPosition(x,y,angle);
    qDebug() << "GET POSITION " << pos;

    qreal A = b2 * sind(angle);
    qreal B = b2 * cosd(angle);
    //удлинение шпинделя
    qreal xsh = pos.x() - B;
    qreal ysh = pos.y() - A;
    qreal lensh = sqrt(pow(xsh-x,2)+pow(ysh-y,2)) - lenshdef;
    lensh = lensh - upFrom;
    newSteps1 = getStepsD1(lensh);

    qreal angmv = atand(pos.x()/pos.y()) - angdef;
    qreal ramaLen = lenghtRama(angmv);
    newSteps34 = getStepsD34(ramaLen);
    qreal d2Len = lenghtVal(angle + angmv);
    newSteps2 = getStepsD2(d2Len);

    qDebug() << "Steps: (1)" << newSteps1
             << "(2)" << newSteps2
             << "(34)" << newSteps34;
    if(moveDev && _first) {
        move12D(newSteps1 - _stepVec[0],
                newSteps2 - _stepVec[1],
                newSteps34 - _stepVec[2],
                newSteps34 - _stepVec[3]);
        _first = false;
    }
    else {
        move12(newSteps1 - _stepVec[0],
                newSteps2 - _stepVec[1],
                newSteps34 - _stepVec[2],
                newSteps34 - _stepVec[3]);
    }
    _stepVec[0]=newSteps1;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
}

void DriverPort::setXCoordinate(qreal x)
{
    qreal y = 1550;
    qreal angle = 0;
    qDebug() << "setCoordinate:" << x;
    qreal upZbort = 2.2;
    qreal hp = 1326.1;
    qreal h = 2045;
    qreal k = -85;
    qreal angdef = atand(k/h);
    qreal lenshdef = 170+67+110;
//    qreal upFrom = 5;
    qreal b2 = 118;
    qint32 newSteps1;
    qint32 newSteps2;
    qint32 newSteps34;

    QPointF posRole = getPosRole(x, hp, hp+upZbort, 0);
//    QPoint angRole = getAngleRole(x);
    QPointF pos = getPos3(posRole.x(),y,angle,h);
//    QPointF pos = getPosition(x,y,angle);


    qreal A = b2 * sind(angle);
//    qreal B = b2 * cosd(angle);
    //удлинение шпинделя
//    qreal xsh = pos.x() - B;
    qreal ysh = pos.y() - A;
    qreal lensh = ysh - y - lenshdef;
    newSteps1 = getStepsD1(lensh);

    qreal angmv = atand(pos.x()/pos.y()) - angdef;
    qreal ramaLen = lenghtRama(angmv);
    newSteps34 = getStepsD34(ramaLen);
    qreal d2Len = lenghtVal(angle + angmv);
    newSteps2 = getStepsD2(d2Len);

    qDebug() << "Steps: (1)" << newSteps1
             << "(2)" << newSteps2
             << "(34)" << newSteps34;

    move12(0,
            newSteps2 - _stepVec[1],
            newSteps34 - _stepVec[2],
            newSteps34 - _stepVec[3]);
//    _stepVec[0]=newSteps1;
    _stepVec[1]=newSteps2;
    _stepVec[2]=newSteps34;
    _stepVec[3]=newSteps34;
    sendAllSteps();
}


//void DriverPort::setXCoordinate(qreal x)
//{
//    qreal y = 1600;
//    qreal angle = 0;
//    qDebug() << "setCoordinate:" << x;
//    qreal h = 2045;
//    qreal k = -85;
//    qreal angdef = atand(k/h);
//    qint32 newSteps2;
//    qint32 newSteps34;

//    QPointF pos = getPosition(x,y,angle);

//    qreal angmv = atand(pos.x()/pos.y()) - angdef;
//    qreal ramaLen = lenghtRama(angmv);
//    newSteps34 = getStepsD34(ramaLen);
//    qreal d2Len = lenghtVal(angle + angmv);
//    newSteps2 = getStepsD2(d2Len);

//    qDebug() << "Steps: (2)" << newSteps2
//             << "(34)" << newSteps34;
//    move12(0,
//            newSteps2 - _stepVec[1],
//            newSteps34 - _stepVec[2],
//            newSteps34 - _stepVec[3]);
////    _stepVec[0]=newSteps1;
//    _stepVec[1]=newSteps2;
//    _stepVec[2]=newSteps34;
//    _stepVec[3]=newSteps34;
//}

quint8 DriverPort::moveOnProbe(qint32 speed)
{
    QByteArray data;
    data.append(char(14));
    auto addStep = [&data](qint32 stepD) {
        if (stepD > 0) {
            data.append(char(0x01));
        }
        else {
            data.append(char(0x00));
            stepD = -stepD;
        }
        quint32 step = stepD;
        data.append(char(step>>24));
        data.append(char(step>>16));
        data.append(char(step>>8));
        data.append(char(step));
    };
    addStep(speed);
    return WriteToPort(data);
}

QPointF DriverPort::getPosRole(qreal ux, qreal hp, qreal zlvl, qreal upFrom)
{
//    qreal rl = 5;
    qreal rr = 10;

//    qreal ll = 41.0/2.0-rl;
    qreal lr = 41.0/2.0-rr;

    qreal y = ((pow(900,2)-pow(ux,2))/(4*1172.5))*pow(1.006,((ux*-1)/900))+hp;

    qreal ang = 0;
    if (ux != 0) {
        ang = atand(-ux/2340);
    }

    qreal A=upFrom*sind(ang);//x
    qreal B=upFrom*cosd(ang);//y
    qreal lvlz = y-(rr-rr*cosd(ang)) + B;
    qreal uxu = ux - lr - rr*sind(ang) - A;
    if (lvlz < (zlvl + upFrom)) {
        lvlz = zlvl + upFrom;
    }
    return QPointF(uxu,lvlz);
    //    ret = [uxu, lvlz, ang];
}

qreal DriverPort::getAngleRole(qreal ux)
{
    qreal ang = 0;
    if (ux != 0) {
        ang = atand(-ux/2340);
    }
    return ang;
}

QPointF DriverPort::getPos3(qreal x1, qreal y1, qreal ang, qreal hhh)
{
   qreal h = hhh;//1982.8;//2065.5;//радиус рамы к перпендикуляру болта крепления шпинделя
   qreal k = 85; //вынос болта от центра рамы
   qreal r = sqrt(pow(h,2)+pow(k,2));
   if(ang != 0) {
       //!!! С углом непроверена правильность перемещения
       k=tand(90+ang);
       qreal B=118*cosd(ang);
       //!!!FIXME есть более просое вычисление значений с меньшими степенями и умножениями
       //подстановкой в исходные констант
       x1=x1+B;
       qreal c = pow((-k*x1),2)-2*k*x1*y1+pow(y1,2)-pow(r,2);
       qreal a=pow(k,2)+1;
       qreal b=(-2*pow(k,2)*x1+2*k*y1);
       qreal D = pow(b,2)-4*a*c;
       qreal xpos = (-b+sqrt(D))/(2*a);
       qreal xneg = (-b-sqrt(D))/(2*a);
       qreal ypos=k*(xpos-x1)+y1;
       qreal yneg=k*(xneg-x1)+y1;
       //ret = [xpos ypos xneg yneg];
       if(ypos >= 0) {
           return QPointF(xpos,ypos);
       } else {
           return QPointF(xneg,yneg);
       }
   } else {
       qreal B=118*cosd(ang);
       //!!!FIXME есть более просое вычисление значений с меньшими степенями и умножениями
       //подстановкой в исходные констант
       x1=x1+B;
       qreal xpos = x1;
       qreal xneg = x1;
       qreal ypos=sqrt(pow(r,2)-pow(xpos,2));
       qreal yneg=sqrt(pow(r,2)-pow(xneg,2));
       //ret = [xpos ypos xneg yneg];
       if(ypos >= 0) {
           return QPointF(xpos,ypos);
       } else {
           return QPointF(xneg,yneg);
       }
   }
}

qint32 DriverPort::getStepsD1(qreal val)
{
    val = -val;
    qreal ldef = 5; //удлинение на оборот
    qreal reduce = 1/10.0;
    qreal rstep = 1.8;
    qreal rotsteps = 360/rstep;
    return (val/ldef)*(rotsteps/reduce);
}

qint32 DriverPort::getStepsD2(qreal val)
{
//    val = -val;
    qreal ldef = 5; //удлинение на оборот
    qreal reduce = 1/10.0;
    qreal reduce2 = 0.84;
    qreal rstep = 1.8;
    qreal rotsteps = 360/rstep;
    return (val/ldef)*(rotsteps/(reduce*reduce2));
}

qint32 DriverPort::getStepsD34(qreal val)
{
//    val = -val;
    qreal ldef = 5; //удлинение на оборот
    qreal reduce = 1/10.0;
    qreal rstep = 1.8;
    qreal rotsteps = 360/rstep;
    return (val/ldef)*(rotsteps/reduce);
}

qreal DriverPort::lenghtRama(qreal ang)
{
    qreal h = 1194;
    qreal lh = 155;
    qreal ldh = 1468;
    qreal ldv = 585.23;

    qreal ldg = sqrt(pow(ldh,2) + pow(ldv,2));
    qreal lhg = sqrt(pow(h,2) + pow(lh,2));

    qreal ang1 = acosd(ldh/ldg);
    qreal ang2 = acosd(h/lhg);
    qreal ang_dr = 90 - ang1 - ang2;

    qreal cub = pow(ldg,2) + pow(lhg,2);
    qreal cos = 2*ldg*lhg*cosd(ang_dr);
    qreal ldr = sqrt(cub-cos);
    cos = 2*ldg*lhg*cosd(ang_dr+ang);
    qreal len = sqrt(cub-cos);

    return ldr - len;
}

qreal DriverPort::lenghtVal(qreal ang)
{
    qreal ldg = 608.281;
    qreal lhg = 608.595;

    qreal ang1 = 1.743;
    qreal ang2 = 26.129;
    qreal ang_dr = 90 - ang1 - ang2;

    qreal cub = pow(ldg,2) + pow(lhg,2);
    qreal cos = 2*ldg*lhg*cosd(ang_dr);
    qreal ldr = sqrt(cub-cos);
    cos = 2*ldg*lhg*cosd(ang_dr+ang);
    qreal len = sqrt(cub-cos);
    return ldr - len;
}

QPointF DriverPort::getPosition(qreal x1, qreal y1, qreal angle)
{
    qreal h = 2045;
    qreal k = 85;
    x1 += 5;
    qreal r = sqrt(pow(h,2) + pow(k,2));
    if (angle != 0) {
        k = tand(90 + angle);
        qreal bb = 118*cosd(angle);
        x1 = x1 + bb;

        qreal kx = -k * x1;
        kx = qPow(kx,2);
        qreal kxy = k*x1;
        kxy = kxy * y1;
        kxy *= 2;
        qreal c = kx - kxy + pow(y1,2) - pow(r,2);
        qreal a = pow(k,2) + 1;
        qreal b = (-2 * pow(k,2) * x1 + 2 * k * y1);
        qreal D = pow(b,2) - 4*a*c;
        qreal xPos = (-b+sqrt(D))/(2 * a);
        qreal xNeg = (-b-sqrt(D))/(2 * a);
        qreal yPos = k * (xPos - x1) + y1;
        qreal yNeg = k * (xNeg - x1) + y1;
        if(yPos >= 0) {
            return QPointF{xPos, yPos};
        } else {
            return QPointF{xNeg, yNeg};
        }
    } else {
        qreal bb = 118*cosd(angle);
        //!!!FIXME есть более просое вычисление значений с меньшими степенями и умножениями
        //подстановкой в исходные констант
        x1 = x1 + bb;
        qreal xPos = x1;
        qreal xNeg = x1;
        qreal yPos = sqrt(pow(r,2) - pow(xPos,2));
        qreal yNeg = sqrt(pow(r,2) - pow(xNeg,2));
        //ret = [xpos ypos xneg yneg];
        if(yPos >= 0) {
            return QPointF{xPos, yPos};
        } else {
            return QPointF{xNeg, yNeg};
        }
    }
}

void DriverPort::move12(qint32 s1, qint32 s2, qint32 s3, qint32 s4)
{
    if(!s1 && !s2 && !s3 && !s4)
        return;
//    s3 = -s3;
//    s4 = -s4;
    _move = true;
    QByteArray data;
    data.append(char(12));
    auto addStep = [&data](qint32 stepD) {
        if (stepD > 0) {
            data.append(char(0x00));
        }
        else {
            data.append(char(0x01));
            stepD = -stepD;
        }
        quint32 step = stepD;
        data.append(char(step>>24));
        data.append(char(step>>16));
        data.append(char(step>>8));
        data.append(char(step));
    };
    addStep(s1);
    addStep(s2);
    addStep(s3);
    addStep(s4);
    WriteToPort(data);
}

void DriverPort::move12D(qint32 s1, qint32 s2, qint32 s3, qint32 s4)
{
    if(!s1 && !s2 && !s3 && !s4)
        return;
//    s3 = -s3;
//    s4 = -s4;
    _move = true;
    QByteArray data;
    data.append(char(16));
    auto addStep = [&data](qint32 stepD) {
        if (stepD > 0) {
            data.append(char(0x00));
        }
        else {
            data.append(char(0x01));
            stepD = -stepD;
        }
        quint32 step = stepD;
        data.append(char(step>>24));
        data.append(char(step>>16));
        data.append(char(step>>8));
        data.append(char(step));
    };
    addStep(s1);
    addStep(s2);
    addStep(s3);
    addStep(s4);
    sendAllSteps();
    WriteToPort(data);
}

void DriverPort::move12mm(qreal m1, qreal m2, qreal m3, qreal m4)
{
    if(!m1 && !m2 && !m3 && !m4)
        return;
    m1 *=400;
    m2 *=476.19;
    m3 *=400;
    m4 *=400;
    _move = true;
    QByteArray data;
    data.append(char(12));
    auto addStep = [&data](qint32 stepD) {
        if (stepD > 0) {
            data.append(char(0x00));
        }
        else {
            data.append(char(0x01));
            stepD = -stepD;
        }
        quint32 step = stepD;
        data.append(char(step>>24));
        data.append(char(step>>16));
        data.append(char(step>>8));
        data.append(char(step));
    };
    addStep(m1);
    addStep(m2);
    addStep(m3);
    addStep(m4);
    sendAllSteps();
//    qDebug() << data;
    WriteToPort(data);
}

void DriverPort::sendAllSteps()
{
    emit sendStep(quint8(0),_stepVec[0]);
    emit sendStep(quint8(1),_stepVec[1]);
    emit sendStep(quint8(2),_stepVec[2]);
    emit sendStep(quint8(3),_stepVec[3]);
}

void DriverPort::ReadInPort(){
    QByteArray data;
    data.append(thisPort.readAll());
    _move = false;
    emit driveStop(data[0]);

    outPort(data);

//    ((QString)(adr.toInt())).toLatin1().toHex()
}

void DriverPort::setStep12(qint32 s1, qint32 s2, qint32 s3, qint32 s4)
{
    qint32 posS1 = s1;
    qint32 posS2 = s2;
    qint32 posS3 = s3;
    qint32 posS4 = s4;
    s1 = s1 - _stepVec[0];
    s2 = s2 - _stepVec[1];
    s3 = s3 - _stepVec[2];
    s4 = s4 - _stepVec[3];
    _stepVec[0] = posS1;
    _stepVec[1] = posS2;
    _stepVec[2] = posS3;
    _stepVec[3] = posS4;
    if(!s1 && !s2 && !s3 && !s4)
        return;
//    s3 = -s3;
//    s4 = -s4;
    _move = true;
    QByteArray data;
    data.append(char(12));
    auto addStep = [this,&data](qint32 stepD) {
        if (stepD > 0) {
            data.append(char(0x00));
        }
        else {
            data.append(char(0x01));
            stepD = -stepD;
        }
        quint32 step = stepD;
        data.append(char(step>>24));
        data.append(char(step>>16));
        data.append(char(step>>8));
        data.append(char(step));
    };
    addStep(s1);
    addStep(s2);
    addStep(s3);
    addStep(s4);
    sendAllSteps();
    WriteToPort(data);
}
