#ifndef SPINBOXMEMORABLE_H
#define SPINBOXMEMORABLE_H

#include <QSpinBox>

class SpinBoxMemorable : public QSpinBox
{
    Q_OBJECT
public:
    SpinBoxMemorable(QWidget *parent = nullptr);
    int prevValue();

public Q_SLOTS:
    void setValueMem();
    void setValueWithSave(int val);

signals:
    void editingFinishedWithSave();
private:
    int _prevValue = -2147483648;
    int _helper = -2147483648;
};

#endif // SPINBOXMEMORABLE_H
