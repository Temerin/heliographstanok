#include "toothtenzwriter.h"
#include <QDebug>
#include <QPoint>

ToothTenzWriter::ToothTenzWriter(QObject *parent) : LogWriter(parent)
{
    makeFile();
}

void ToothTenzWriter::newFile()
{
    _file.close();
    makeFile();
}

void ToothTenzWriter::writeLog(QPoint xy, QMap<int, int> toothTenz)
{
    _writeStream << "X" << xy.x() << " Y" << xy.y() << " ";
    foreach (const auto tooth, toothTenz.keys()) {
        _writeStream << "Th" << tooth << " Tz" << toothTenz.value(tooth) << " ";
    }
    _writeStream << "\n";
    _writeStream.flush();
}

void ToothTenzWriter::makeFile()
{
    QDateTime dataTime = QDateTime::currentDateTimeUtc();
    _file.setFileName("./ToothTenz" +
                     QString::number(dataTime.date().day()) + "_" +
                     QString::number(dataTime.date().month()) + "_" +
                     QString::number(dataTime.date().year()) + "_" +
                     QString::number(dataTime.time().hour()) + "_" +
                     QString::number(dataTime.time().minute()) + ".tt");
    if(!_file.open(QFile::WriteOnly | QFile::Text))
        qDebug() << "Ne otkrivayetsa file davleniy";
}
