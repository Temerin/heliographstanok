#ifndef ENCODERPORT_H
#define ENCODERPORT_H

#include <QObject>
#include <QVector>
#include "port.h"

class EncoderPort : public Port
{
    Q_OBJECT
public:
    EncoderPort();

    QVector<quint32> _encVec{0,0,0,0};
//    quint32 _calibLenght[4][2] = {{,},
//                                  {,},
//                                  {,},
//                                  {,}};

//    qint32 _tenzLimitMax = 300;
//    qint32 _tenzLimitMin = -400;
    qint32 _tenzHardLimit = -3000;
    quint32 _tooth = 0;
    quint32 _prevTooth = 0;
    quint16 _rev = 0;
    qint32 _tenz1 = 0;
    qint32 _tenz2 = 0;
    bool _probe = false;
    bool _move1 = false;
    bool _move2 = false;
    bool _move3 = false;
    bool _move4 = false;
    quint8 _counter1 = 0;
    quint8 _counter2 = 0;
    quint8 _counter3 = 0;
    quint8 _counter4 = 0;
    bool _devMove = false;

    qint32 getKg();
    qreal getDeg2();
    qreal getDeg34();
    qreal toMm(quint8 axis);
    qreal toMm(quint8 axis, quint32 enc);
    qreal toMmFromNull(quint8 axis);
signals:

    void toothTenz(int tth, qint32 tnz);
    void tenzLimit(bool over);
    void tenzHardLimit();
    void encode(quint32 enc);
    void tooth(quint32 tooth);
    void rev(int rev);
    void tenz1(int tenz);
    void tenz2(int tenz);
    void tenzAver(qint32 aver, qint32 max, quint8 maxTooth, qint32 min, quint8 minTooth);
    void probe(bool probe);
    void stop1();
    void stop2();
    void stop3();
    void stop4();
    void move1();
    void move2();
    void move3();
    void move4();

public slots:

    void ReadInPort();

private:
    qint32 _counter = 0;
    quint8 _toothCounter = 0;
    qint32 _tenzToothCounter = 0;
    qint32 _maxTenz = -2147483648;
    quint8 _maxTooth = 0;
    qint32 _minTenz = 2147483647;
    quint8 _minTooth = 0;
    qint32 _toothRev = 0; //Зуб, на котором обновился оборот
    QVector<qint32> _tenzTable;

    void setTenzTable();

};

#endif // ENCODERPORT_H
