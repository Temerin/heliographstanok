#ifndef DRIVERPORT_H
#define DRIVERPORT_H

#include <QObject>
#include "port.h"
#include <QPointF>
#include <QVector>

class DriverPort : public Port
{
    Q_OBJECT
public:
    DriverPort();

    qint32 toStep(quint8 axis, qreal mm);
    qreal toMm(quint8 axis, qint32 step);


    QVector<qint32> _stepVec{0,0,0,0};

    QByteArray _prevCom;

    quint8 _numCom = 1;

    bool _first = true;

    bool _move = false;

    qreal upZbort = 3.4;

public slots:

    void setStep12(qint32 s1, qint32 s2, qint32 s3, qint32 s4);
    void setX34(qreal x);
    void setX(qreal x);
    void setXABolt(qreal x, qreal angle);
    void setXA(qreal x, qreal angle);
    void setXASaveY(qreal x, qreal angle);
    void setXYASaveY(qreal x, qreal y, qreal angle);
    void setA(qreal angle34, qreal angle2);
    void setCoordinate(qreal x, qreal y, qreal angle, qreal upFrom);
    void setCoordinateA(qreal x, qreal y, qreal angle, qreal upFrom);
    void setXCoordinate(qreal x);
    quint8 moveOnProbe(qint32 speed);


    QPointF getPosRole(qreal ux, qreal hp, qreal zlvl, qreal upFrom);
    qreal getAngleRole(qreal ux);
    QPointF getPos3(qreal x1, qreal y1, qreal ang, qreal hhh);
    QPointF getPosition(qreal x1, qreal y1, qreal angle);
    void move12(qint32 s1=0, qint32 s2=0, qint32 s3=0, qint32 s4=0);
    void move12D(qint32 s1=0, qint32 s2=0, qint32 s3=0, qint32 s4=0);
    void move12mm(qreal m1, qreal m2, qreal m3, qreal m4);

    void sendAllSteps();

    void move(int step=1, int speed=1, qint8 mode=1);
    void moveDrive(quint8 mode=1, quint8 foward=1, quint32 step = 0);
    void stop();
    qint32 getSignStepFromAngle2(qreal angle);
    qint32 getSignStepFromAngle34(qreal angle);
    void sendBack();
    void setNull();
    quint8 WriteToPort(QByteArray data);

    qint32 getSignStepFromAngle2New(qreal angle);
    qint32 getSignStepFromAngle34New(qreal angle);
    qint32 getStepsD1(qreal val);
    qint32 getStepsD2(qreal val);
    qint32 getStepsD34(qreal val);
    qreal lenghtRama(qreal ang);
    qreal lenghtVal(qreal ang);
    void ReadInPort();
signals:
    void stop1();
    void stop2();
    void stop3();
    void stop4();
    void move1();
    void move2();
    void move3();
    void move4();
    void sendStep(quint8 axis, qint32 steps);

};

#endif // DRIVERPORT_H
