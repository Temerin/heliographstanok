#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QToolButton>

class ColorButton : public QToolButton
{
    Q_OBJECT
public:
    ColorButton(const QColor &color, const QString &text = "", QWidget *parent = 0);

    void setColor(const QColor &color);
    QColor color();
private:
    QColor _color;
};

#endif // COLORBUTTON_H
