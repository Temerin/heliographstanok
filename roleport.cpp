#include "roleport.h"
#include "crc.h"
#include <QDebug>
#include <QTimer>

RolePort::RolePort()
{
    _timer = new QTimer(this);
    _timer->setInterval(1500);
    connect(_timer, &QTimer::timeout, this, &RolePort::pol);
    _timer->start();
    connect(&thisPort, &QSerialPort::readyRead,this,&RolePort::ReadInPort);
}

void RolePort::move(int step, int speed, qint8 mode)
{
    Q_UNUSED(step)
    QByteArray data; // Текстовая переменная
    data.append(char(0x01));
    data.append(char(0x10));
    data.append(char(0x18));
    data.append(char(0x70));
    data.append(char(0x00));
    data.append(char(0x02));
    data.append(char(0x04));
    int intMode = 0;
    switch (mode) {
    case 1:
        intMode = 1<<10;
        break;
    case 2:
        intMode = (1<<10) | (1<<9);
    default:
        break;
    }
    data.append(char(intMode>>8));
    data.append(char(intMode));
    data.append(char(speed>>8));
    data.append(char(speed));
    WriteToPort(data);
}

void RolePort::stop()
{
    move(0,0,0);
}

void RolePort::clear()
{
    int speed = 100;
    QByteArray data; // Текстовая переменная
    data.append(char(0x01));
    data.append(char(0x10));
    data.append(char(0x18));
    data.append(char(0x70));
    data.append(char(0x00));
    data.append(char(0x02));
    data.append(char(0x04));
    int intMode = 1<<13;
    data.append(char(intMode>>8));
    data.append(char(intMode));
    data.append(char(speed>>8));
    data.append(char(speed));
    WriteToPort(data);
}

void RolePort::WriteToPort(QByteArray data)
{
//    qDebug() << data.toHex();
    if(thisPort.isOpen()) {
//        qDebug() << thisPort.portName() << ": " << data.toHex();
        uint16_t crc = CRC_16((uint8_t *)data.constData(),data.count());
        data.append(crc);
        data.append(crc >> 8);
        thisPort.write(data);
        inPort(data);
    }
}

void RolePort::pol()
{
    QByteArray data;
    data.clear();
    data.append(char(0x01));
    data.append(char(0x03));
    data.append(char(0x18));
    data.append(char(0x75));
    data.append(char(0x00));
    data.append(char(0x05));

    WriteToPort(data);
}

void RolePort::ReadInPort(){
    QByteArray data;
    data.append(thisPort.readAll());
//    qDebug()  << data.toHex();

    switch (_readMode) {
    case 1:{
        uint16_t newState = 0;
        newState |= (quint8)data[3];
        newState = newState << 8;
        newState |= (quint8)data[4];
        _prevState = newState;

        uint16_t newFreq = 0;
        newFreq |= (quint8)data[5];
        newFreq = newFreq << 8;
        newFreq |= (quint8)data[6];
        _prevFreq = newFreq;

        uint16_t newCurrent = 0;
        newCurrent |= (quint8)data[7];
        newCurrent = newCurrent << 8;
        newCurrent |= (quint8)data[8];
        _prevCurrent = newCurrent;

        uint16_t newAlarm = 0;
        newAlarm |= (quint8)data[11];
        newAlarm = newAlarm << 8;
        newAlarm |= (quint8)data[12];
        _prevAlarm = newAlarm;

//        _readMode = 0;
    }
        break;
    default:
        break;
    }

    outPort(data);

}

void RolePort::GetError()
{
    QByteArray data;
    data.clear();
    data.append(char(0x01));
    data.append(char(0x03));
    data.append(char(0x18));
    data.append(char(0x75));
    data.append(char(0x00));
    data.append(char(0x05));
    WriteToPort(data);
    _readMode = 1;
}
