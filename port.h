#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

struct Settings {
    QString name;
    qint32 baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
};

class Port : public QObject
{
    Q_OBJECT

public:

    explicit Port(QObject *parent = 0);

    ~Port();

    QSerialPort thisPort;

    Settings SettingsPort;

//    bool _move = false;
    bool clr = false;

//    uint32_t enc = 0;

signals:

    void finished_Port(); //

    void error_(QString err);

    void outPort(QByteArray data);

    void inPort(QByteArray data);

    void connected(bool con);

    void driveStop(quint8 numCom);

//    void encode(uint32_t enc);

public slots:

    void ReadInPort();

    void disconnectPort();

    void connectPort(void);

    void writeSettingsPort(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);

    void processPort();

    void WriteToPort(QByteArray data);




private slots:

    void handleError(QSerialPort::SerialPortError error);//

public:

};

#endif // PORT_H
