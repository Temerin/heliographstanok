#ifndef TENZGRADIENT_H
#define TENZGRADIENT_H

#include <QRgb>
#include <QMap>

class TenzGradient
{
public:
    TenzGradient();

    void addColor(int tenz, QRgb color);
    QRgb getColor(int tenz);
    void clearColors();

private:
    QMap<int,QRgb> _tenzColorMap;
};

#endif // TENZGRADIENT_H
