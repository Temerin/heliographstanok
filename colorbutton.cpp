#include "colorbutton.h"
#include <QDebug>

ColorButton::ColorButton(const QColor &color, const QString &text, QWidget *parent)
    : QToolButton(parent)
{
//    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setColor(color);
    setText(text);
}

void ColorButton::setColor(const QColor &color)
{
    _color = color;
//    Пример "background:rgb(200,100,150);"
    QString stSh = "background:rgb("
            + QString::number(color.red()) + ","
            + QString::number(color.green()) + ","
            + QString::number(color.blue()) + ");";
    setStyleSheet(stSh);
}

QColor ColorButton::color()
{
    return _color;
}
