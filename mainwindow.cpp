#include "mainwindow.h"
#include <QMessageBox>
#include <QString>
#include <QDesktopWidget>
#include <QDesktopWidget>
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>
#include <unistd.h>
#include <errno.h>
#include "ui_mainwindow.h"
#include <QThread>
#include <QDebug>
#include <QTimer>
#include <QPoint>
#include <QFileDialog>
#include "driverport.h"
#include "roleport.h"
#include "encoderport.h"
#include "gcodeform.h"
#include <cstdlib>
#include <QTime>
#include <QDateTime>
#include "logwriter.h"
#include "mathplus.h"
#include "colorbutton.h"
#include "spinboxmemorable.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
    ,ui(new Ui::MainWindow)
    ,_calibData(std::make_unique<QMap<QPoint,uint32_t>>())
{
    ui->setupUi(this);
	
    _gCode = new GCodeForm();


    _logWriter = new LogWriter(this);

    QFile file("./calib.txt");
    parseCalib(file);

    QTimer *timerRef = new QTimer(this);
    timerRef->setInterval(500);

    _timerPaint = new QTimer(this);
    _timerPaint->setInterval(500);
    ui->LayColor->setAlignment(Qt::AlignTop);

    ui->comBox_1->addItem(_settings.value("comBox_1").toString());
    ui->comBox_2->addItem(_settings.value("comBox_2").toString());
    ui->comBox_3->addItem(_settings.value("comBox_3").toString());
    ui->sbTenzHardLimit->setValue(_settings.value("TenzLimit",-3000).toInt());
    on_pbLoadSettings_clicked();
    on_pbLoadSettings_2_clicked();

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    auto createEncoderPort = [this](QString name, int baudrate, int DataBits,
            int Parity, int StopBits, int FlowControl) {
        QThread *thread_New = new QThread;//Создаем поток для порта платы
        EncoderPort *portNew = new EncoderPort();//Создаем обьект по классу
        portNew->moveToThread(thread_New);//помешаем класс  в поток
        portNew->thisPort.moveToThread(thread_New);//Помещаем сам порт в поток
        portNew->writeSettingsPort(name, baudrate, DataBits, Parity, StopBits, FlowControl);
        connect(portNew, &Port::error_, this, &MainWindow::print);//Лог ошибок
        connect(thread_New, &QThread::started, portNew, &Port::processPort);//Переназначения метода run
        connect(portNew, &Port::finished_Port, thread_New, &QThread::quit);//Переназначение метода выход
        connect(thread_New, &QThread::finished, portNew, &Port::deleteLater);//Удалить к чертям поток
        connect(portNew, &Port::finished_Port, thread_New, &QThread::deleteLater);//Удалить к чертям поток
        connect(ui->BtnConnect, &QPushButton::clicked, portNew, &Port::connectPort);
        connect(ui->BtnDisconect, &QPushButton::clicked, portNew, &Port::disconnectPort);
        connect(portNew, &EncoderPort::outPort, this, [portNew,this](QByteArray data) {
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " >> ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        connect(portNew, &EncoderPort::inPort, this, [portNew,this](QByteArray data) {
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " << ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        connect(portNew, &EncoderPort::toothTenz,_scene1,[this](int tth, qint32 tnz) {
            auto it = _tenzToothMap.insert(tth, tnz);
            if (it != _tenzToothMap.end())
                if (((it + 1).value() == 11111)
                        && ((it + 1).key() < (it.key() + 11)))
                    _tenzToothMap.remove((it + 1).key());  //Удаляем не валидные значения рядом, если они есть
            if (it != _tenzToothMap.begin())
                if (((it - 1).value() == 11111)
                        && ((it - 1).key() > (it.key() - 11)))
                    _tenzToothMap.remove((it + 1).key());
        });
        connect(portNew, &EncoderPort::rev,_scene1,[this]() {
            _scene1->printCyrcleTenz(QPoint(static_cast<int>(-(_oldX+0.5f)),
                                            static_cast<int>(_gCode->getY(_oldX) + 0.5f)), _tenzToothMap);
            _scene1->update();
            _ttWriter->writeLog(QPoint(static_cast<int>(-(_oldX+0.5f)),
                                       static_cast<int>(_gCode->getY(_oldX) + 0.5f)), _tenzToothMap);
//            _tenzToothVec.clear();
        });
        thread_New->start();
        portNew->_tenzHardLimit = _settings.value("TenzLimit",-3000).toInt();
        return portNew;
    };

    auto createDriverPort = [this](QString name, int baudrate, int DataBits,
            int Parity, int StopBits, int FlowControl) {
        QThread *thread_New = new QThread;//Создаем поток для порта платы
        DriverPort *portNew = new DriverPort();//Создаем обьект по классу
        portNew->moveToThread(thread_New);//помешаем класс  в поток
        portNew->thisPort.moveToThread(thread_New);//Помещаем сам порт в поток
        portNew->writeSettingsPort(name, baudrate, DataBits, Parity, StopBits, FlowControl);
        connect(portNew, &Port::error_, this, &MainWindow::print);//Лог ошибок
        connect(thread_New, &QThread::started, portNew, &Port::processPort);//Переназначения метода run
        connect(portNew, &Port::finished_Port, thread_New, &QThread::quit);//Переназначение метода выход
        connect(thread_New, &QThread::finished, portNew, &Port::deleteLater);//Удалить к чертям поток
        connect(portNew, &Port::finished_Port, thread_New, &QThread::deleteLater);//Удалить к чертям поток
        connect(ui->BtnConnect, &QPushButton::clicked, portNew, &Port::connectPort);
        connect(ui->BtnDisconect, &QPushButton::clicked, portNew, &Port::disconnectPort);
        connect(this, &MainWindow::move, portNew, &DriverPort::move);
        connect(this, &MainWindow::moveDrive, portNew, &DriverPort::moveDrive);
        connect(this, &MainWindow::setCoordinate, portNew, &DriverPort::setCoordinate);
        connect(this, &MainWindow::setXCoordinate, portNew, &DriverPort::setXCoordinate);
        connect(this, &MainWindow::moveOnProbe, portNew, &DriverPort::moveOnProbe);
        connect(this, &MainWindow::move12, portNew, &DriverPort::move12);
        connect(this, &MainWindow::setStep12, portNew, &DriverPort::setStep12);
        connect(portNew, &DriverPort::sendStep, this, [this,portNew](quint8 axis, qint32 steps) {
            switch (axis) {
            case 0:
                ui->sbSetStepD1->setValue(steps);
                ui->lbD1Mm->setText(QString::number(portNew->toMm(axis,steps)+_offset,'f',2));
                _settings.setValue("CurrentPosition/D1",steps);
                break;
            case 1:
                ui->sbSetStepD2->setValue(steps);
                ui->lbD2Mm->setText(QString::number(portNew->toMm(axis,steps),'f',2));
                _settings.setValue("CurrentPosition/D2",steps);
                break;
            case 2:
                ui->sbSetStepD3->setValue(steps);
                ui->lbD3Mm->setText(QString::number(portNew->toMm(axis,steps),'f',2));
                _settings.setValue("CurrentPosition/D3",steps);
                break;
            case 3:
                ui->sbSetStepD4->setValue(steps);
                ui->lbD4Mm->setText(QString::number(portNew->toMm(axis,steps),'f',2));
                _settings.setValue("CurrentPosition/D4",steps);
                break;
            default:
                break;
            }
        });
        connect(portNew, &Port::outPort, this, [portNew,this](QByteArray data) { //????
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " >> ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        connect(portNew, &Port::inPort, this, [portNew,this](QByteArray data) { //????
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " << ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        thread_New->start();
        return portNew;
    };

    auto createRolePort = [this](QString name, int baudrate, int DataBits,
            int Parity, int StopBits, int FlowControl) {
        QThread *thread_New = new QThread;//Создаем поток для порта платы
        RolePort *portNew = new RolePort();//Создаем обьект по классу
        portNew->moveToThread(thread_New);//помешаем класс  в поток
        portNew->thisPort.moveToThread(thread_New);//Помещаем сам порт в поток
        portNew->writeSettingsPort(name, baudrate, DataBits, Parity, StopBits, FlowControl);
        connect(portNew, &Port::error_, this, &MainWindow::print);//Лог ошибок
        connect(thread_New, &QThread::started, portNew, &Port::processPort);//Переназначения метода run
        connect(portNew, &Port::finished_Port, thread_New, &QThread::quit);//Переназначение метода выход
        connect(thread_New, &QThread::finished, portNew, &Port::deleteLater);//Удалить к чертям поток
        connect(portNew, &Port::finished_Port, thread_New, &QThread::deleteLater);//Удалить к чертям поток
        connect(ui->BtnConnect, &QPushButton::clicked, portNew, &Port::connectPort);
        connect(ui->BtnDisconect, &QPushButton::clicked, portNew, &Port::disconnectPort);
        connect(this, &MainWindow::role, portNew, &RolePort::move);
        connect(this, &MainWindow::clear, portNew, &RolePort::clear);
        connect(portNew, &Port::outPort, this, [portNew,this](QByteArray data) {
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " >> ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        connect(portNew, &EncoderPort::inPort, this, [portNew,this](QByteArray data) {
            if (data.length() == 0)
                return;
            QString str = portNew->thisPort.portName();
            str += " << ";
            str += data.toHex();
            _logWriter->writeLog(str);
        });
        thread_New->start();
        return portNew;
    };

    connect(ui->BtnSave, &QPushButton::clicked, this, [this,createRolePort,
            createDriverPort,createEncoderPort,timerRef]() {
        _rolePort = createRolePort(ui->comBox_1->currentText(), 19200, 8, 0, 1, 0);
        _driverPort = createDriverPort(ui->comBox_2->currentText(), 115200, 8, 0, 1, 0);
        _encoderPort = createEncoderPort(ui->comBox_3->currentText(), 115200, 8, 0, 1, 0);

        connect(_gCode, &GCodeForm::parable, _driverPort,[this](qreal x, qreal y, qreal angle) {
            qreal upFrom = ui->dSbFromUp->value();
            _driverPort->upZbort = ui->dSbUpZbort->value();
            switch (_roleMode) {
            case 0:
                emit setCoordinate(x,y,angle,upFrom);
                break;
            case 1:{
//                qreal xOld = x;
//                qreal yOld = y;
                qreal hp = 1428.6;
                qreal lvlZ;
                if (x <= 0)
                    lvlZ = y - 10.5* sind(angle) - 10+10*cosd(angle);
                else
                    lvlZ = y - 15.5*(-sind(angle))-5+5*cosd(angle); //отрицательный угол

                if (lvlZ >= hp)
                    emit setCoordinateA(x,y,angle,upFrom);
                else {
                    if((x >= -500) && (x <= 500)) {

                    } else {
                        y = hp;
                    }

                    if(x <= 0) {
                        x = x-15.5-5*sind(angle);
                        y = y-5+5*cosd(angle);
                    } else {
                        x = x+10.5+10*sind(angle);
                        y = y-10+10*cosd(angle);
                    }
                    if (y < hp)
                        y = hp;
                    emit setCoordinateA(x,y,0,upFrom+5000);
                }
            }
                break;
            default:
                break;
            }

        });
        connect(_driverPort, &Port::driveStop,  this, [this](quint8 val) {
            Q_UNUSED(val)
            comparePosition();
        });
        connect(_encoderPort, &EncoderPort::tenz1, this, [this](qint32 data) {
            ui->lcdNumTenz->display(QString::number(data));
        });
        connect(_encoderPort, &EncoderPort::tenzAver, this, [this](qint32 aver,
                                                                   qint32 max, quint8 maxTooth,
                                                                   qint32 min, quint8 minTooth) {
            ui->lcdNumTenzAver->display(QString::number(aver));
            ui->lcdNumTenzMax->display(QString::number(max));
            ui->lcdNumTenzMin->display(QString::number(min));
            ui->lbMinTenz->setText("Мин зуб " + QString::number(minTooth));
            ui->lbMaxTenz->setText("Макс зуб " + QString::number(maxTooth));
            if (ui->checkBox->isChecked()) {
                if (aver < ui->sbTenz_2->value())
                    on_pbUpD1_clicked();
                if (aver > ui->sbTenz->value())
                    on_pbDownD1_clicked();
            }
        });
        connect(_encoderPort, &EncoderPort::rev, this, [this](quint32 data) {
            if (_findNull) {
                emit role(1,0,0);
                _findNull = false;
            }
            ui->labelRev->setText(QString::number(data));
        });
        connect(_encoderPort, &EncoderPort::tenzHardLimit, this, [this]() {
            on_pbStop_clicked();
        });
        connect(_encoderPort, &EncoderPort::probe, this, [this](bool data) {
            ui->labelProbe->setText(QString::number(int(data)));
        });

        connect(timerRef,&QTimer::timeout,this,[this]() {
            if((_encoderPort->_encVec[0] > 88000) && (_encoderPort->_encVec[0] < 150000))
                emit move(0,0,0);
            ui->labelE1->setText(QString::number(_encoderPort->_encVec[0]) + " ("
                    + QString::number(_encoderPort->toMmFromNull(1),'f',2) + ")");
            ui->labelE2->setText(QString::number(_encoderPort->_encVec[1]) + " ("
                    + QString::number(_encoderPort->toMmFromNull(2),'f',2) + ")");
            ui->labelE3->setText(QString::number(_encoderPort->_encVec[2]) + " ("
                    + QString::number(_encoderPort->toMmFromNull(3),'f',2) + ")");
            ui->labelE4->setText(QString::number(_encoderPort->_encVec[3]) + " ("
                    + QString::number(_encoderPort->toMmFromNull(4),'f',2) + ")");
            ui->labelT1->setText(QString::number(_encoderPort->_tenz1));

            ui->labelTooth->setText(QString::number(_encoderPort->_tooth));
//            ui->labelT12->setText(QString::number((_encoderPort->getKg())));
            ui->labelAngle2->setText(QString::number(_encoderPort->getDeg2(),'f',2) + '('
                    + QString::number(_encoderPort->getDeg2() - _encoderPort->getDeg34(),'f',2)
                    + ')');
            ui->labelAngle34->setText(QString::number(_encoderPort->getDeg34(),'f',2));

            //Повоотное
            ui->labelStatus->setText(QString::number(_rolePort->_prevState));
            ui->labelFreq->setText(QString::number(_rolePort->_prevFreq));
            ui->labelCurrent->setText(QString::number(_rolePort->_prevCurrent));
            ui->labelAlarm->setText(QString::number(_rolePort->_prevAlarm));
            QTime time;
            time.setHMS(0,0,0);
            _deltaTime = time.addMSecs(QDateTime::currentDateTimeUtc().toMSecsSinceEpoch()
                                                                  - _timeStart.toMSecsSinceEpoch());

//            if (_startCatalka || _meter) {
//                ui->label_4->setText("Время: " + _deltaTime.toUTC().time().toString());
//            }
            if (ui->BtnMeter_3->isChecked()) { // обмер v2
                qint32 lineAll = 244 / ui->sbStepT->value() + 1;
                ui->label_4->setText("Время: " + _deltaTime.toString());
                qint32 allStepsLine = (ui->dSbStop_2->value() - _startX)  //Всего шагов в проходе
                                  / ui->dsbStepM->value();
                qint32 setStepsLine = (_x - _startX)  //Сколько шагов сделано
                                  / ui->dsbStepM->value();

                QTime timeLineMeter;
                if (_timeLineMeter.msecsSinceStartOfDay() != 0) {
                    timeLineMeter = _timeLineMeter;
                } else {
                    if (setStepsLine)
                        timeLineMeter = time.addMSecs(_deltaTime.msecsSinceStartOfDay()
                                                      / setStepsLine
                                                      * allStepsLine
                                                      + 300000);
                    else return;
                }
                _timeLeft = time.addMSecs((timeLineMeter.msecsSinceStartOfDay() - 300000)
                                          / allStepsLine
                                          * (allStepsLine - setStepsLine)
                                          + 300000     //Примерно пять минут возврат в ноль при текущей скорости
                                          + ((lineAll - _lineCounter) * timeLineMeter.msecsSinceStartOfDay()));
                ui->label_4->setText(ui->label_4->text() + "/" + _timeLeft.toString());

                return;
            }
            if ((_startCatalka) || (_meter)) {
                ui->label_4->setText("Время: " + _deltaTime.toString());
                if (ui->progressBar->value()) {
                    _timeLeft = time.addMSecs(_deltaTime.msecsSinceStartOfDay()
                                              * (ui->progressBar->maximum() - ui->progressBar->value())
                                              / ui->progressBar->value());
                    ui->label_4->setText(ui->label_4->text() + "/" + _timeLeft.toString());
                }
                return;
            }
        });
        timerRef->start();

        _settings.setValue("comBox_1",ui->comBox_1->currentText());
        _settings.setValue("comBox_2",ui->comBox_2->currentText());
        _settings.setValue("comBox_3",ui->comBox_3->currentText());
    });
    ui->tabWidget->setCurrentIndex(_settings.value("CurrentMainPage",0).toInt());
    createScene();

    on_BtnConnect_clicked(); //Автоподключение

    ui->tabWidget_2->setCurrentIndex(_settings.value("CurrentDisplayPage",1).toInt());

    _driverPort->_stepVec[0] = _settings.value("CurrentPosition/D1",0).toInt();
    _driverPort->_stepVec[1] = _settings.value("CurrentPosition/D2",0).toInt();
    _driverPort->_stepVec[2] = _settings.value("CurrentPosition/D3",0).toInt();
    _driverPort->_stepVec[3] = _settings.value("CurrentPosition/D4",0).toInt();
    ui->sbSetStepD1->setValue(_driverPort->_stepVec[0]);
    ui->sbSetStepD2->setValue(_driverPort->_stepVec[1]);
    ui->sbSetStepD3->setValue(_driverPort->_stepVec[2]);
    ui->sbSetStepD4->setValue(_driverPort->_stepVec[3]);

    if (_settings.value("TimerPaint",false).toBool()) {
        on_chbPaint_clicked(true);
        ui->chbPaint->setChecked(true);
    }
    if (_settings.value("PaintNewFile",false).toBool())
        ui->chbPaintNewFile->setChecked(true);
    loadTenzColorTible(_tenzColorFilePath);
}
//++++++++[Процедура закрытия приложения]+++++++++++++++++++++++++++++++++++++++++++++
MainWindow::~MainWindow()
{
    emit stopAll();
    _meter = false;
    delete _scene1;
    delete ui; //Удаление формы
}

void MainWindow::loadTenzColorTible(const QString &filePath)
{
    emit cleanColor();
    _tenzColorMap.clear();
    QFile file(filePath);
    if (file.open((QFile::ReadOnly | QFile::Text))) {
        QTextStream in(&file);
        int tenz  = 0;
        quint8 r = 0;
        quint8 g = 0;
        quint8 b = 0;
        auto deleteSpace = [](QString &line) { //Удаляет подстроку + пробелы после неё
            line.remove(0,line.indexOf(' ')+1);
            while((line[0] == ' ')&&(!line.isNull())) {
                line.remove(0,1);
            }
        };
        while (!in.atEnd()) {
            QString line = in.readLine();
            tenz = line.left(line.indexOf(' ')).toInt();
            deleteSpace(line);
            r = line.left(line.indexOf(' ')).toUShort();
            deleteSpace(line);
            g = line.left(line.indexOf(' ')).toUShort();
            deleteSpace(line);
            b = line.left(line.indexOf(' ')).toUShort();
            addColor(tenz,QColor(r, g, b));
        }
    } else {
        print("Файл настройки цветов не открылся");
    }
}

void MainWindow::addColor(int tenz, QColor color)
{
    auto sbTenz  = new SpinBoxMemorable();
    sbTenz->setRange(-50000,50000);
    sbTenz->setValueWithSave(tenz);
    auto bnColor = new ColorButton(color);
    connect(bnColor,&ColorButton::clicked, ui->LayColor, [this,bnColor,sbTenz]() {
        QColor color = QColorDialog::getColor(bnColor->color());
        if (color.isValid()) {
             bnColor->setColor(color);
             _tenzColorMap[sbTenz->value()] = color;
        }
    });
    auto tbDelete = new QToolButton();
    tbDelete->setIcon(QIcon(":/resources/del.png"));
    auto deleteColor = [this,bnColor,sbTenz,tbDelete]() {
        _tenzColorMap.remove(sbTenz->value());
        ui->LayColor->removeWidget(sbTenz);
        delete sbTenz;
        ui->LayColor->removeWidget(bnColor);
        delete bnColor;
        ui->LayColor->removeWidget(tbDelete);
        delete tbDelete;
    };
    connect(tbDelete,&QToolButton::clicked, ui->LayColor, deleteColor);
    connect(this,&MainWindow::cleanColor, sbTenz, deleteColor);
    connect(sbTenz,&SpinBoxMemorable::editingFinishedWithSave, ui->LayColor, [this,bnColor,sbTenz]() {
        _tenzColorMap.remove(sbTenz->prevValue());
        _tenzColorMap.insert(sbTenz->value(),bnColor->color());
    });
    ui->LayColor->addWidget(sbTenz, ui->LayColor->rowCount(),0);
    ui->LayColor->addWidget(bnColor,ui->LayColor->rowCount()-1,1);
    ui->LayColor->addWidget(tbDelete,ui->LayColor->rowCount()-1,2);
    _tenzColorMap.insert(tenz,color);
}

void MainWindow::comparePosition()
{
    QString stWarning = "background:rgb(255,0,0);";
    QString stDefault = "background:rgb(0,255,0);";
    if (compare(_encoderPort->toMmFromNull(1),
                _driverPort->toMm(1,ui->sbSetStepD1->value()) + _offset))
        ui->lbD1Mm->setStyleSheet(stDefault);
    else
        ui->lbD1Mm->setStyleSheet(stWarning);

    if (compare(_encoderPort->toMmFromNull(2),
                _driverPort->toMm(2,ui->sbSetStepD2->value())))
        ui->lbD2Mm->setStyleSheet(stDefault);
    else
        ui->lbD2Mm->setStyleSheet(stWarning);

    if (compare(_encoderPort->toMmFromNull(3),
                _driverPort->toMm(3,ui->sbSetStepD3->value())))
        ui->lbD3Mm->setStyleSheet(stDefault);
    else
        ui->lbD3Mm->setStyleSheet(stWarning);

    if (compare(_encoderPort->toMmFromNull(4),
                _driverPort->toMm(4,ui->sbSetStepD4->value())))
        ui->lbD4Mm->setStyleSheet(stDefault);
    else
        ui->lbD4Mm->setStyleSheet(stWarning);
}

void MainWindow::updatePosDriveMm()
{
    qint32 steps = ui->sbSetStepD1->value();
    ui->lbD1Mm->setText(QString::number(_driverPort->toMm(1,steps) + _offset,'f',2));
    _settings.setValue("CurrentPosition/D1",steps);

    steps = ui->sbSetStepD2->value();
    ui->lbD2Mm->setText(QString::number(_driverPort->toMm(2,steps),'f',2));
    _settings.setValue("CurrentPosition/D2",steps);

    steps = ui->sbSetStepD3->value();
    ui->lbD3Mm->setText(QString::number(_driverPort->toMm(2,steps),'f',2));
    _settings.setValue("CurrentPosition/D3",steps);

    steps = ui->sbSetStepD4->value();
    ui->lbD4Mm->setText(QString::number(_driverPort->toMm(4,steps),'f',2));
    _settings.setValue("CurrentPosition/D4",steps);
    comparePosition();
}

//++++++++[Процедура пределения подключенных портов]+++++++++++++++++++++++++++++++++++
void MainWindow::on_Btn_Search_clicked()
{
    ui->comBox_1->clear();
    ui->comBox_2->clear();
    ui->comBox_3->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->comBox_1->addItem(info.portName());
        ui->comBox_2->addItem(info.portName());
        ui->comBox_3->addItem(info.portName());
    }
}


//+++++++++++++[Процедура ввода данных из строки]++++++++++++++++++++++++++++++++++++++++
void MainWindow::enterRole()
{
    emit role(1,_speed, _mode);
}
//+++++++++++++[Процедура вывода данных в консоль]++++++++++++++++++++++++++++++++++++++++
void MainWindow::print(QString data)
{
    qDebug() << data.toUtf8();
    _logWriter->writeLog(data);
    ui->consol->textCursor().insertText(data.toUtf8() + "\n"); // Вывод текста в консоль
    ui->consol->moveCursor(QTextCursor::End);//Scroll
}

void MainWindow::wait(int msecs)
{
    QEventLoop loop;
    QTimer::singleShot(msecs,&loop,&QEventLoop::quit);
    loop.exec();
}

void MainWindow::waitStop(int axis)
{
    QTime timeCounterAll;
    QTimer timer;
    timer.setInterval(5000);
    timeCounterAll.start();
    wait(100);
    QEventLoop loop;
    connect(this, &MainWindow::stopAll,  &loop, [&loop]() {
        loop.quit();
    });
    qDebug() << "waitStop";
    switch (axis) {
    case 1:
        if(_encoderPort->_move1) {
            connect(_encoderPort, &EncoderPort::stop1, &loop, &QEventLoop::quit);
            loop.exec();
        }
        break;
    case 2:
        if(_encoderPort->_move2) {
            connect(_encoderPort, &EncoderPort::stop2, &loop, &QEventLoop::quit);
            loop.exec();
        }
        break;
    case 3:
        if(_encoderPort->_move3) {
            connect(_encoderPort, &EncoderPort::stop3, &loop, &QEventLoop::quit);
            loop.exec();
        }
        break;
    case 4:
        if(_encoderPort->_move4) {
            connect(_encoderPort, &EncoderPort::stop4, &loop, &QEventLoop::quit);
            loop.exec();
        }
        break;
    case 5: //Поворотное //Сюда бы дополнительное прерывание каким-то сигналом остановки
        connect(_encoderPort, &EncoderPort::tooth, &loop,[this, &loop](quint32 tooth) {
            if (tooth >= _toothCounter) {
                emit role(1,0,0);
                loop.quit();
            }
        });
        connect(&timer,&QTimer::timeout,this,[this]() {
            print("Повторная отправка");
            emit clear();
            wait(200);
            emit role(1,_speed, _mode);
        });
        timer.start();
        loop.exec();
        qDebug() << "Gotovo" << timeCounterAll.elapsed();
        break;
    }
}

void MainWindow::waitAns(quint8 ans)
{
    QEventLoop loop;
    connect(this, &MainWindow::stopAll,  &loop, [&loop]() {
        loop.quit();
    });
    wait(50);
    if (_driverPort->_move) {
        connect(_driverPort, &Port::driveStop,  &loop, [&loop,ans](quint8 val) {
            Q_UNUSED(ans)
            Q_UNUSED(val)
//            if (val == ans)
            loop.quit();
        });
        loop.exec();
    }
}

bool MainWindow::roleStop()
{
    bool stop = true;
    connect(_encoderPort, &EncoderPort::tooth, this, [&stop]() {
        stop = false;
    });
    wait(350);
    qDebug() << "УСПЕШНО ДОЖДАЛИСЬ" << stop;
    return stop;
}

bool MainWindow::allObjInit()
{
    return _driverPort
            && _encoderPort
            && _rolePort;
}

void MainWindow::createScene()
{
    _ttWriter = new ToothTenzWriter(this);
    _scene1 = new Scene3D(this);
    _scene1->setWindowTitle("Давление");
    _scene1->resize(500, 500);

    _scene1->clearToothMatrix();

    for (int i = 1; i < 244; i+=12) {  //Начальное заполнение
        _tenzToothMap.insert(i,11111); //Просто число, которое не встретится
    }

    QGLFormat frmt; // создать формат по умолчанию
    frmt.setDoubleBuffer(true); // задать двойную буферизацию
    frmt.setSwapInterval(1);

    connect(this, &MainWindow::newSetX,_scene1,[this](qreal newX) {
        _scene1->printCyrcleTenz(QPoint(-_oldX, _gCode->getY(_oldX)), _tenzToothMap);
        _scene1->update();
        _ttWriter->writeLog(QPoint(_oldX, _gCode->getY(_oldX)), _tenzToothMap);
        _tenzToothMap.clear();
        _oldX = newX;
        for (int i = 1; i < 244; i+=12) {
            _tenzToothMap.insert(i,11111); //Просто число, которое не встретится //Можно добавить прозрачности
        }
    });
    connect(this, &MainWindow::cleanMatrix,_scene1,[this]() {
        _scene1->clearToothMatrix();
    });
    connect(_timerPaint, &QTimer::timeout, _scene1,[this]() {
        _scene1->printCyrcleTenz(QPoint(-_oldX, _gCode->getY(_oldX)), _tenzToothMap);
        _scene1->update();
    });
    ui->sceneLayout->addWidget(_scene1);
}

void MainWindow::logWrite(QPoint point, uint32_t enc)
{
    if (!allObjInit()) {
        return;
    }
    quint32 tooth = (_encoderPort->_tooth - _startTooth) % 244;
    _writeStream << tooth << ";" << point.x() << ";"
                 << _encoderPort->_encVec[0] << ";" << _encoderPort->_encVec[1] << ";"
                 << _encoderPort->_encVec[2] << ";" << _encoderPort->_encVec[3] << ";"
                 << _encoderPort->_rev << ";"       << _encoderPort->_tooth << ";"
                 << _encoderPort->_tenz1 << ";"     << _encoderPort->_tenz2 << ";"
                 << "\n";
    ui->consol->textCursor().insertText(QString::number(point.x()) + ' ' + QString::number(point.y()) + ' ' + QString::number(enc) + '\n'); // Вывод текста в консоль
    ui->consol->moveCursor(QTextCursor::End);//Scroll
    ++_counter;
    if(_counter > 6) {
        _writeStream.flush();
        _counter = 0;
    }
}

void MainWindow::parseCalib(QFile &file)
{
    _calibData->clear();


    if (file.open((QFile::ReadOnly | QFile::Text))) {
        QTextStream in(&file);
        //Сюда бы reserve от количества строк в файле

        auto deleteSpace = [](QString &line) { //Удаляет подстроку + пробелы после неё
            line.remove(0,line.indexOf(' ')+1);
            while((line[0] == ' ')&&(!line.isNull())) {
                line.remove(0,1);
            }
        };
        QPoint point;
        while (!in.atEnd()) {
            QString line = in.readLine();
            auto substring = line.left(line.indexOf(' '));
            substring.remove('X');
            if (substring.toInt() != point.x()) {

                point.setX(substring.toInt());
            }
            deleteSpace(line);
            substring = line.left(line.indexOf(' '));
            substring.remove('Y');
            point.setY(substring.toInt());
            deleteSpace(line);
            substring = line.left(line.indexOf(' '));
            substring.remove('E');
            _calibData->insert(point, substring.toInt());
        }
    }

    for(int z = 1; z < 123; ++z) {
        for(int x = 0; x < 3162; ++x) {
            _calibData->insert(QPoint(z,x), rand() % 10);
        }
    }
}

void MainWindow::on_Btn_Gcode_clicked()
{
    _gCode = new GCodeForm(/*_calibData.get(),*/0,
                               ui->doubleSpinBoxStep->value(),this);
}

void MainWindow::on_pbRightD1_clicked()
{
    emit moveDrive(2,1,ui->sbStepD1->value());
}

void MainWindow::on_pbLeftD1_clicked()
{
    emit moveDrive(2,0,ui->sbStepD1->value());
}

void MainWindow::on_pbLeftD2_clicked()
{
    emit moveDrive(4,0,ui->sbStepD2->value());
}

void MainWindow::on_pbRightD2_clicked()
{
    emit moveDrive(4,1,ui->sbStepD2->value());
}

void MainWindow::on_pbLeftD3_clicked()
{
    emit moveDrive(6,0,ui->sbStepD3->value());
}

void MainWindow::on_pbRightD3_clicked()
{
    emit moveDrive(6,1,ui->sbStepD3->value());
}

void MainWindow::on_pbLeftD4_clicked()
{
    emit moveDrive(8,0,ui->sbStepD4->value());
}

void MainWindow::on_pbRightD4_clicked()
{
    emit moveDrive(8,1,ui->sbStepD4->value());
}

void MainWindow::on_pbLeftD34_clicked()
{
    emit moveDrive(10,0,ui->sbStepD34->value());
}

void MainWindow::on_pbRightD34_clicked()
{
    emit moveDrive(10,1,ui->sbStepD34->value());
}

void MainWindow::on_pbStop_clicked()
{
    ui->Btn_Gcode_Next_On->setChecked(false);
    ui->BtnMeter->setChecked(false);
    ui->BtnMeter_2->setChecked(false);
    ui->BtnMeter_3->setChecked(false);
    _start = false;
    _meter = false;
    _forceStop = true;
    emit move(0,0,0);
    emit role(1,0,0);
    wait(300);
    on_pbToNull3_clicked();
}

void MainWindow::on_pbFindNull_clicked()
{
    _findNull = true;
    emit role(1,_speed, _mode);

//    _angle2 = 0;
//    _angle34 = 0;
//    emit setNull();
}

void MainWindow::on_Btn_Gcode_Next_clicked()
{
    ui->dSbXG->setValue(_x);
    QString str = "X: " + QString::number(_x);
    _logWriter->writeLog(str);
    _gCode->getNext(_x);
    _x += ui->dSbStepG->value();
    ui->progressBar->setValue(ui->progressBar->value()+1);
    emit newSetX(_x);
}

void MainWindow::on_pbSet_3_clicked()
{
    _gCode->getNext(ui->dSbXG->value());
    _x = ui->dSbXG->value();
    emit newSetX(_x);
}

void MainWindow::on_Btn_Gcode_Next_On_clicked(bool checked)
{
    if (!allObjInit()) {
        ui->Btn_Gcode_Next_On->setChecked(false);
        return;
    }
    if (ui->chbPaintNewFile->isChecked())
        on_pbClearScene_clicked();
    QString str = "Katayem";
    str += "X старт: " + QString::number(ui->dSbXG->value())
            + "Пауза: " + QString::number(ui->sbPause->value())
            + "Режим прохода: " + ui->comboBox->currentText()
            + "Шаг: " + QString::number(ui->dSbStepG->value())
            + "Подъём борта: " + QString::number(ui->dSbUpZbort->value())
            + "Подъём параболы: " + QString::number(ui->dSbStepG->value())
            + "Отступ от матрицы: " + QString::number((ui->dSbFromUp->value()*1000 + _offset) / 1000)
            + "Обороты матрицы: " + QString::number(ui->dsbSpeedRoll->value()) + " Об/м";
    _logWriter->writeLog(str);
    _timeStart = QDateTime::currentDateTimeUtc();
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum((ui->dSbStop->value() - _x) / ui->dSbStepG->value());
    _startCatalka = checked;
    _driverPort->_first = true;
    ui->dSbStepG->setEnabled(!_startCatalka);
    ui->sbPause->setEnabled(!_startCatalka);
    ui->dSbXG->setEnabled(!_startCatalka);
    ui->dSbStop->setEnabled(!_startCatalka);
    ui->dSbFromUp->setEnabled(!_startCatalka);
    ui->dSbUpZbort->setEnabled(!_startCatalka);
    ui->checkBox->setEnabled(_startCatalka);
    while (_startCatalka) {
        on_Btn_Gcode_Next_clicked();
        waitAns(0);
        wait(ui->sbPause->value());
        if (_x <= ui->dSbStop->value())
            _startCatalka = false;
    }
    ui->progressBar->setValue(ui->progressBar->maximum());
}

void MainWindow::on_pbSetCoordinate_clicked()
{
    emit setCoordinate(ui->dSbCoorX->value(),
                       ui->dSbCoorY->value(),
                       ui->dSbCoorA->value(),
                       0);
    emit newSetX(ui->dSbCoorX->value());
}

void MainWindow::on_BtnConnect_clicked()
{
    if (!_driverPort) {
        ui->BtnSave->clicked();
        ui->BtnConnect->clicked();
    }
}

void MainWindow::on_BtnMeter_clicked(bool checked) // Круговой обмер
{
    if (!allObjInit()) {
        ui->BtnMeter->setChecked(false);
        return;
    }
    if (ui->chbPaintNewFile->isChecked())
        on_pbClearScene_clicked();
    QString str = "Круговой обмер";
    str += "X старт: " + QString::number(ui->dSbXMater->value())
            + "Скорость: " + QString::number(ui->sbSpeedT->value())
            + "Шаг: " + QString::number(ui->dsbStepM->value())
            + "Конец: " + QString::number(ui->dSbStop_2->value())
            + "Шаг поворота (зубья): " + QString::number(ui->sbStepT->value())
            + "Подъём вверх (мкм): " + QString::number(ui->dsbYCoor->value());
    _logWriter->writeLog(str);
    _startTooth = _encoderPort->_tooth;
    _timeStart = QDateTime::currentDateTimeUtc();
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum((ui->dSbStop_2->value() - ui->dSbXMater->value())
                                * (244 / ui->sbStepT->value())
                                / ui->dsbStepM->value());
    _meter = checked;
    if (_meter) {
        QDateTime dataTime = QDateTime::currentDateTimeUtc();
        _fileCalib.setFileName("./calib" +
                         QString::number(dataTime.date().day()) + "_" +
                         QString::number(dataTime.date().month()) + "_" +
                         QString::number(dataTime.date().year()) + "_" +
                         QString::number(dataTime.time().hour()) + "_" +
                         QString::number(dataTime.time().minute()) + ".csv");
        if(!_fileCalib.open(QFile::WriteOnly | QFile::Text))
            print("Файл обмера не открылся");
    }
    quint32 startTooth = _encoderPort->_tooth;
    _toothCounter = startTooth;
    qreal currentPos = 0;
    qreal startPos = ui->dSbXMater->value();
    qreal stepCoil = ui->dsbStepM->value();
    qint32 lastCoilPos = (ui->dSbStop_2->value() - startPos)/ stepCoil + 1;
    qreal lastPos = lastCoilPos*244;
    qint32 coil = 0;
    qreal lastX = -1;
    qreal x = 0.0;

//    on_pbSet_4_clicked();
//    waitAns(0);

    coil = (currentPos) / 244;
    x = startPos + coil * stepCoil;
    if (x != lastX) {
        ui->dSbXMater->setValue(x);
        on_pbSet_4_clicked();
        waitAns(0);
        lastX = x;
    }

    while (currentPos < lastPos) { //Алярма. Нужна доп проверка перед смещением по ИКС
        if(!_meter)
            break;

        emit moveOnProbe(ui->sbSpeedT->value()); // Опуститься до заготовки
        waitAns(0);
        logWrite(QPoint(_x,_encoderPort->_tooth),_encoderPort->_encVec[0]);//Запомнить значения
        emit move12(400);  //подняться вверх
        waitAns(0);

        coil = (currentPos + ui->sbStepT->value()) / 244;
        x = startPos + coil * stepCoil;
        if (x != lastX) {
            ui->dSbXMater->setValue(x);
            on_pbSet_4_clicked();
            waitAns(0);
            lastX = x;
        }
        _toothCounter += ui->sbStepT->value();
        emit role(1,_speed, _mode);
        waitStop(5);
        while(!roleStop()) {
            emit role(1,0,0);
        }
        currentPos = _encoderPort->_tooth - startTooth;
        ui->progressBar->setValue(ui->progressBar->value()+1);
    }
    _fileCalib.close();
    _meter = false;
    ui->progressBar->setValue(ui->progressBar->maximum());
}

void MainWindow::on_pbSet_4_clicked()
{
    if (!allObjInit()) {
        return;
    }
    if (_x < ui->dSbBort->value()) {
        emit move12(ui->dsbYCoor->value() / 10 * 8);
    } else {
        emit move12(ui->dsbYCoor->value() / 10 * 4);  //подняться вверх
    }
    waitAns(0);
    _x = ui->dSbXMater->value();
    QString str = "X: " + QString::number(_x);
    _logWriter->writeLog(str);
    emit setXCoordinate(_x);
    emit newSetX(_x);
}

void MainWindow::on_pbSetStep_clicked()
{
    emit setStep12(ui->sbStep1->value(),
                   ui->sbStep2->value(),
                   ui->sbStep34->value(),
                   ui->sbStep34->value());
}


void MainWindow::on_pbClear_clicked()
{
    emit clear();
}

void MainWindow::on_BtnMeter_2_clicked(bool checked)
{
    if (!allObjInit()) {
        ui->BtnMeter_2->setChecked(false);
        return;
    }
    if (ui->chbPaintNewFile->isChecked())
        on_pbClearScene_clicked();
    QString str = "Линейный обмер";
    str += "X старт: " + QString::number(ui->dSbXMater->value())
            + "Скорость: " + QString::number(ui->sbSpeedT->value())
            + "Шаг: " + QString::number(ui->dsbStepM->value())
            + "Конец: " + QString::number(ui->dSbStop_2->value())
            + "Подъём вверх (мкм): " + QString::number(ui->dsbYCoor->value());
    _logWriter->writeLog(str);
    _startTooth = _encoderPort->_tooth;
    _timeStart = QDateTime::currentDateTimeUtc();
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum((ui->dSbStop_2->value() - ui->dSbXMater->value())
                                / ui->dsbStepM->value());
    _meter = checked;
    if (_meter) {
        QDateTime dataTime = QDateTime::currentDateTimeUtc();
        _fileCalib.setFileName("./calib" +
                         QString::number(dataTime.date().day()) + "_" +
                         QString::number(dataTime.date().month()) + "_" +
                         QString::number(dataTime.date().year()) + "_" +
                         QString::number(dataTime.time().hour()) + "_" +
                         QString::number(dataTime.time().minute()) + ".csv");
        if (!_fileCalib.open(QFile::WriteOnly | QFile::Text))
            print("Файл обмера не открылся");
    }
    quint32 startTooth = _encoderPort->_tooth;
    _toothCounter = startTooth;
    qreal currentPos = 0;
    qreal startPos = ui->dSbXMater->value();
    qreal stepCoil = ui->dsbStepM->value();
    qint32 lastCoilPos = (ui->dSbStop_2->value() - startPos)/ stepCoil + 1;
    qreal lastPos = lastCoilPos*244;
    qint32 coil = 0;
    qreal lastX = -1;
    qreal x = 0.0;

    coil = (currentPos) / 244;
    x = startPos + coil * stepCoil;
    if (x != lastX) {
        ui->dSbXMater->setValue(x);
        on_pbSet_4_clicked();
        waitAns(0);
        lastX = x;
    }

    while (currentPos < lastPos) { //Алярма. Нужна доп проверка перед смещением по ИКС
        if (!_meter)
            break;

        emit moveOnProbe(ui->sbSpeedT->value()); // Опуститься до заготовки
        waitAns(0);
        logWrite(QPoint(_x,_encoderPort->_tooth),_encoderPort->_encVec[0]);//Запомнить значения
//        emit move12(400);  //подняться вверх
//        waitAns(0);

        coil = (currentPos + 244) / 244;
        x = startPos + coil * stepCoil;
        if (x != lastX) {
            ui->dSbXMater->setValue(x);
            on_pbSet_4_clicked();
            waitAns(0);
            lastX = x;
        }
        _toothCounter += 244;
        currentPos =_toothCounter - startTooth;
        ui->progressBar->setValue(ui->progressBar->value()+1);
    }
    _fileCalib.close();
    _meter = false;
    ui->progressBar->setValue(ui->progressBar->maximum());
}

void MainWindow::on_pbSetSteps_clicked()
{
    if (!allObjInit()) {
        return;
    }
    _driverPort->_stepVec[0] = ui->sbSetStepD1->value();    
    _driverPort->_stepVec[1] = ui->sbSetStepD2->value();
    _driverPort->_stepVec[2] = ui->sbSetStepD3->value();
    _driverPort->_stepVec[3] = ui->sbSetStepD4->value();
    _settings.setValue("CurrentPosition/D1",_driverPort->_stepVec[0]);
    _settings.setValue("CurrentPosition/D2",_driverPort->_stepVec[1]);
    _settings.setValue("CurrentPosition/D3",_driverPort->_stepVec[2]);
    _settings.setValue("CurrentPosition/D4",_driverPort->_stepVec[3]);
    updatePosDriveMm();
}

void MainWindow::on_BtnMeter_3_clicked(bool checked) //Круговой v2
{
    _meter = checked;
    if (!allObjInit()) {
        ui->BtnMeter_3->setChecked(false);
        return;
    }
    if (ui->chbPaintNewFile->isChecked())
        on_pbClearScene_clicked();
    on_pbFindNull_clicked();
    while (_findNull) { //Пока ищем ноль
        if (!_meter)
            break;
        wait(500);
    }
    QString str = "Круговой обмер v2";
    str += "X старт: " + QString::number(ui->dSbXMater->value())
            + "Скорость: " + QString::number(ui->sbSpeedT->value())
            + "Шаг: " + QString::number(ui->dsbStepM->value())
            + "Конец: " + QString::number(ui->dSbStop_2->value())
            + "Шаг поворота (зубья): " + QString::number(ui->sbStepT->value())
            + "Подъём вверх (мкм): " + QString::number(ui->dsbYCoor->value());
    _lineCounter = 0;
    _logWriter->writeLog(str);
    _timeStart = QDateTime::currentDateTimeUtc();
    _timeLineMeter.setHMS(0,0,0);
    _startTooth = _encoderPort->_tooth;
    ui->progressBar->setValue(0);
    ui->LabelLineText->setText("Проход");
    ui->labelLine->setText("0");
    ui->progressBar->setMaximum((ui->dSbStop_2->value() - ui->dSbXMater->value())
                                * (244 / ui->sbStepT->value())
                                / ui->dsbStepM->value());
    if (_meter) {
        QDateTime dataTime = QDateTime::currentDateTimeUtc();
        _fileCalib.setFileName("./calib" +
                         QString::number(dataTime.date().day()) + "_" +
                         QString::number(dataTime.date().month()) + "_" +
                         QString::number(dataTime.date().year()) + "_" +
                         QString::number(dataTime.time().hour()) + "_" +
                         QString::number(dataTime.time().minute()) + ".csv");
        if(!_fileCalib.open(QFile::WriteOnly | QFile::Text))
            print("Файл обмера не открылся");
    }
    quint32 startTooth = _encoderPort->_tooth;
    _toothCounter = startTooth;
    while (_encoderPort->_tooth < startTooth+244) {
        ++_lineCounter;
        ui->labelLine->setText(QString::number(_lineCounter));
        QTime time;
        time.setHMS(0,0,0); //Проблема была в этом. Обязательная строчка.
        if (_lineCounter > 1)
            _timeLineMeter = time.addMSecs(_deltaTime.msecsSinceStartOfDay() / _lineCounter-1);
        _startX = ui->dSbXMater->value();
        if (!_meter)
            break;
        qreal currentPos = 0;
        qreal startPos = ui->dSbXMater->value();
        qreal stepCoil = ui->dsbStepM->value();
        qint32 lastCoilPos = (ui->dSbStop_2->value() - startPos)/ stepCoil + 1;
        qreal lastPos = lastCoilPos*244;
        qint32 coil = 0;
        qreal lastX = -1;
        qreal x = 0.0;

        coil = (currentPos) / 244;
        x = startPos + coil * stepCoil;
        if (x != lastX) {
            ui->dSbXMater->setValue(x);
            on_pbSet_4_clicked();
            waitAns(0);
            lastX = x;
        }

        while (currentPos < lastPos) { //Алярма. Нужна доп проверка перед смещением по ИКС
            if (!_meter)
                break;

            emit moveOnProbe(ui->sbSpeedT->value()); // Опуститься до заготовки
            waitAns(0);
            logWrite(QPoint(_x,_encoderPort->_tooth),_encoderPort->_encVec[0]);//Запомнить значения
    //        emit move12(400);  //подняться вверх
    //        waitAns(0);

            coil += 1;
            x = startPos + coil * stepCoil;
            if (x != lastX) {
                ui->dSbXMater->setValue(x);
                on_pbSet_4_clicked();
                waitAns(0);
                lastX = x;
            }
            currentPos += 244;
            ui->progressBar->setValue(ui->progressBar->value()+1);
        }
        if (!_meter)
            break;
        ui->dSbXMater->setValue(_startX);
        emit move12(40000);  //подняться вверх
        waitAns(0);
        _toothCounter += ui->sbStepT->value();
        emit role(1,_speed, _mode);
        waitStop(5);
        while (!roleStop()) {
            emit role(1,0,0);
        }
    }
    _fileCalib.close();
    _meter = false;
    ui->progressBar->setValue(ui->progressBar->maximum());
    ui->labelLine->setText(" ");
    ui->LabelLineText->setText(" ");
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    _roleMode = (RoleMod)index;
}

void MainWindow::on_pbUpD1_clicked()
{
    if (!allObjInit()) {
        return;
    }
    qint32 steps = (ui->sbMkmD1->value() / 10) * 4;
    _offset += ui->sbMkmD1->value();
    qreal fUp = (ui->dSbFromUp->value()*1000 + _offset) / 1000;
    ui->label_38->setText("Отступ " + QString::number(_offset) + " мкм\n ("
                          + QString::number(fUp)
                          + " мм)");
    QString str = ui->label_38->text();
    _logWriter->writeLog(str);
    if (!_startCatalka) {
        emit move12(steps);
        waitAns(0);
    } else {
        _driverPort->_stepVec[0] -= steps;
    }
}

void MainWindow::on_pbDownD1_clicked()
{
    if (!allObjInit()) {
        return;
    }
    qint32 steps = (ui->sbMkmD1->value() / 10) * -4;
    _offset -= ui->sbMkmD1->value();
    qreal fUp = (ui->dSbFromUp->value()*1000 + _offset) / 1000;
    ui->label_38->setText("Отступ " + QString::number(_offset) + " мкм\n ("
                          + QString::number(fUp)
                          + " мм)");
    QString str = ui->label_38->text();
    _logWriter->writeLog(str);
    if (!_startCatalka) {
        emit move12(steps);
        waitAns(0);
    } else {
        _driverPort->_stepVec[0] -= steps;
    }
}

void MainWindow::on_pbSaveSettings_clicked()
{
    QString indexStr = QString::number(ui->cbMeterProfile->currentIndex());
    _settings.setValue("Meter/" + indexStr + "/X",ui->dSbXMater->value());
    _settings.setValue("Meter/" + indexStr + "/Speed",ui->sbSpeedT->value());
    _settings.setValue("Meter/" + indexStr + "/Bort",ui->dSbBort->value());
    _settings.setValue("Meter/" + indexStr + "/Stop",ui->dSbStop_2->value());
    _settings.setValue("Meter/" + indexStr + "/StepM",ui->dsbStepM->value());
    _settings.setValue("Meter/" + indexStr + "/StepT",ui->sbStepT->value());
    _settings.setValue("Meter/" + indexStr + "/Up",ui->dsbYCoor->value());
    _settings.setValue("Meter/" + indexStr + "/SpeedRpll",ui->dsbSpeedRoll->value());
    _settings.setValue("Meter/" + indexStr + "/TenzHardLimit",ui->sbTenzHardLimit->value());
}

void MainWindow::on_pbLoadSettings_clicked()
{
    QString indexStr = QString::number(ui->cbMeterProfile->currentIndex());
    ui->dSbXMater->      setValue(_settings.value("Meter/" + indexStr + "/X",-53.0).toDouble());
    ui->sbSpeedT->       setValue(_settings.value("Meter/" + indexStr + "/Speed",1000).toInt());
    ui->dSbBort->        setValue(_settings.value("Meter/" + indexStr + "/Bort",-800.0).toDouble());
    ui->dSbStop_2->      setValue(_settings.value("Meter/" + indexStr + "/Stop",-960.0).toDouble());
    ui->dsbStepM->       setValue(_settings.value("Meter/" + indexStr + "/StepM",-1.0).toDouble());
    ui->sbStepT->        setValue(_settings.value("Meter/" + indexStr + "/StepT",30).toInt());
    ui->dsbYCoor->       setValue(_settings.value("Meter/" + indexStr + "/Up",500).toDouble());
    ui->dsbSpeedRoll->   setValue(_settings.value("Meter/" + indexStr + "/SpeedRoll",1.5).toDouble());
    ui->sbTenzHardLimit->setValue(_settings.value("Meter/" + indexStr + "/TenzHardLimit",-1000).toInt());
}

void MainWindow::on_pbSaveSettings_2_clicked()
{
    QString indexStr = QString::number(ui->cbKatkaProfile->currentIndex());
    _settings.setValue("Katka/" + indexStr + "/X",ui->dSbXG->value());
    _settings.setValue("Katka/" + indexStr + "/Mode",ui->comboBox->currentIndex());
    _settings.setValue("Katka/" + indexStr + "/Delay",ui->sbPause->value());
    _settings.setValue("Katka/" + indexStr + "/Step",ui->dSbStepG->value());
    _settings.setValue("Katka/" + indexStr + "/Stop",ui->dSbStop->value());
    _settings.setValue("Katka/" + indexStr + "/UpZBort",ui->dSbUpZbort->value());
    _settings.setValue("Katka/" + indexStr + "/UpFrom",ui->dSbFromUp->value());
    _settings.setValue("Katka/" + indexStr + "/Tenz1",ui->sbTenz->value());
    _settings.setValue("Katka/" + indexStr + "/Tenz2",ui->sbTenz_2->value());
    _settings.setValue("Katka/" + indexStr + "/SpeedRpll",ui->dsbSpeedRoll->value());
    _settings.setValue("Katka/" + indexStr + "/TenzHardLimit",ui->sbTenzHardLimit->value());
}

void MainWindow::on_pbLoadSettings_2_clicked()
{
    QString indexStr = QString::number(ui->cbKatkaProfile->currentIndex());
    ui->dSbXG->          setValue(_settings.value("Katka/" + indexStr + "/X",-153.0).toDouble());
    ui->comboBox->setCurrentIndex(_settings.value("Katka/" + indexStr + "/Mode",0).toInt());
    ui->sbPause->        setValue(_settings.value("Katka/" + indexStr + "/Delay",1000).toInt());
    ui->dSbStepG->       setValue(_settings.value("Katka/" + indexStr + "/Step",-1).toDouble());
    ui->dSbStop->        setValue(_settings.value("Katka/" + indexStr + "/Stop",-960.0).toDouble());
    ui->dSbUpZbort->     setValue(_settings.value("Katka/" + indexStr + "/UpZBort",3.4).toDouble());
    ui->dSbFromUp->      setValue(_settings.value("Katka/" + indexStr + "/UpFrom",8).toDouble());
    ui->sbTenz->         setValue(_settings.value("Katka/" + indexStr + "/Tenz1",-500).toInt());
    ui->sbTenz_2->       setValue(_settings.value("Katka/" + indexStr + "/Tenz2",-700).toInt());
    ui->dsbSpeedRoll->   setValue(_settings.value("Katka/" + indexStr + "/SpeedRoll",1.5).toDouble());
    ui->sbTenzHardLimit->setValue(_settings.value("Katka/" + indexStr + "/TenzHardLimit",-4000).toInt());
}

void MainWindow::on_checkBox_clicked(bool checked)
{
//    ui->label_30->setEnabled(!checked);
//    ui->label_36->setEnabled(!checked);
    QString str = "Включена регулировка по давлению от "
            + QString::number(ui->sbTenz->value()) +
            " до " + QString::number(ui->sbTenz_2->value());
    _logWriter->writeLog(str);
    ui->sbTenz->  setEnabled(!checked);
    ui->sbTenz_2->setEnabled(!checked);
}

void MainWindow::on_sbTenzHardLimit_editingFinished()
{
    if (!allObjInit()) {
        return;
    }
    _encoderPort->_tenzHardLimit = ui->sbTenzHardLimit->value();
    _settings.setValue("TenzLimit",ui->sbTenzHardLimit->value());
//    _scene1->hardLimit = ui->sbTenzHardLimit->value();
//    _scene1->colorTableRefresh();
}

void MainWindow::on_pbHorizont_clicked()
{
    qreal x = _x;
    qreal y = ((810000-pow(x,2))/(4680))*pow(1.006,((x*-1)/900));
    ui->dSbUpZbort->setValue(y);
    ui->dSbStepG->setValue(ui->dSbStepG->value()*3);
}

void MainWindow::on_bpStartRole_clicked()
{
    emit role(1,_speed, 2);
}

void MainWindow::on_pbStartRoleB_clicked()
{
    emit role(1,_speed, 2);
}

void MainWindow::on_pbStopRole_clicked()
{
    emit role(1,_speed, 0);
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    _settings.setValue("CurrentMainPage",index);
}

void MainWindow::on_pbClearScene_clicked()
{
    _ttWriter->newFile();
    emit cleanMatrix();
}

void MainWindow::on_pbClearScene_2_clicked()
{
    if (ui->tabWidget_2->currentIndex() == 0)
        ui->tabWidget_2->setCurrentIndex(1);
    _scene1->setParent(nullptr);
    emit showScene(_scene1);
}

void MainWindow::on_tabWidget_2_currentChanged(int index)
{
//    _scene1->close();
    _settings.setValue("CurrentDisplayPage",index);
    wait(500);
    if (index == 0)
        ui->sceneLayout->addWidget(_scene1);
    _scene1->update();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
    _scene1->close();
}
void MainWindow::on_pbClearScene_3_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(0, "Выбор файла давлений", "", "*.tt");
    QFile file(filePath);
    if (file.open((QFile::ReadOnly | QFile::Text))) {
        QTextStream in(&file);
        //Сюда бы reserve от количества строк в файле
        int x = 0;
        int y = 0;
        int tooth = 0;
        int tenz  = 0;
        auto deleteSpace = [](QString &line) { //Удаляет подстроку + пробелы после неё
            line.remove(0,line.indexOf(' ')+1);
            while((line[0] == ' ')&&(!line.isNull())) {
                line.remove(0,1);
            }
        };
        while (!in.atEnd()) {
            _tenzToothMap.clear();
            QString line = in.readLine();

            line.remove(0,1); //Удалить "X"
            x = line.left(line.indexOf(' ')).toInt();
            deleteSpace(line);

            line.remove(0,1); //Удалить "Y"
            y = line.left(line.indexOf(' ')).toInt();
            deleteSpace(line);

            while (line.contains("Th")) {
                line.remove(0,2); //Удалить "Th"
                tooth = line.left(line.indexOf(' ')).toInt();
                deleteSpace(line);
                line.remove(0,2); //Удалить "Tz"
                tenz  = line.left(line.indexOf(' ')).toInt();
                deleteSpace(line);
                _tenzToothMap.insert(tooth,tenz);
            }
            _scene1->printCyrcleTenz(QPoint(x, y), _tenzToothMap);
            _scene1->update();
        }
    } else {
        print("Файл давлений не открылся");
    }
}

void MainWindow::on_chbPaint_clicked(bool checked)
{
    _settings.setValue("TimerPaint",checked);
    if (checked)
        _timerPaint->start();
    else
        _timerPaint->stop();
}

void MainWindow::returnToNull()
{
    _forceStop = false;
    emit setStep12(-_offset * 4 / 10,
                   ui->sbSetStepD2->value(),
                   ui->sbSetStepD3->value(),
                   ui->sbSetStepD4->value());
    waitAns(0);
    ui->sbSetStepD1->setValue(0);
    _driverPort->_stepVec[0] = ui->sbSetStepD1->value();
    _offset = 0;
    ui->label_38->setText("Отступ " + QString::number(_offset) + " мкм");
    wait(200);
    if (_forceStop)
        return;
    emit setStep12(0,0,0,0);
}

void MainWindow::on_dsbSpeedRoll_valueChanged(double arg1)
{
    _speed = arg1*100;
}


void MainWindow::on_spinBox_valueChanged(int arg1)
{
    _scene1->tickness = arg1;
    _scene1->update();
}

void MainWindow::on_chbMatrix_clicked(bool checked)
{
    _scene1->paintMatrix = checked;
    _scene1->update();
}

void MainWindow::on_pbOpenColor_clicked()
{
    QString filePath =  QFileDialog::getOpenFileName(0, "Выбор файла цветов", "", "*.ini");
    if (filePath.isEmpty())
        return;
    _tenzColorFilePath = filePath;
    loadTenzColorTible(_tenzColorFilePath);
}

void MainWindow::on_pbSaveColor_clicked()
{
    QFile file;
    QTextStream writeStream{&file};
    file.setFileName(_tenzColorFilePath);
    file.remove();
    if(!file.open(QFile::WriteOnly | QFile::Text))
        print("Не открылся файл на запись" + _tenzColorFilePath);
    foreach (const auto tenz, _tenzColorMap.keys()) {
        writeStream << tenz
                    << " " << _tenzColorMap.value(tenz).red()
                    << " " << _tenzColorMap.value(tenz).green()
                    << " " << _tenzColorMap.value(tenz).blue()
                    << "\n";
    }
    writeStream.flush();
    _scene1->colorTableRefresh(_tenzColorFilePath);
}

void MainWindow::on_pbReturnColor_clicked()
{
    loadTenzColorTible(_tenzColorFilePath);
}

void MainWindow::on_pbAddColor_clicked()
{
    addColor(0,QColor(0,0,0));
}

void MainWindow::on_pbToTo_clicked()
{
    emit setStep12(0,
                   -15000,
                   330000,
                   330000);
    //возможно, придется делать дополнительные проверки
}

void MainWindow::on_pbToNull3_clicked()
{
    ui->sbSetStepD1->setValue(
                _driverPort->toStep(1, _encoderPort->toMmFromNull(1)));
    ui->sbSetStepD2->setValue(
                _driverPort->toStep(2, _encoderPort->toMmFromNull(2)));
    ui->sbSetStepD3->setValue(
                _driverPort->toStep(3, _encoderPort->toMmFromNull(3)));
    ui->sbSetStepD4->setValue(
                _driverPort->toStep(4, _encoderPort->toMmFromNull(4)));

    _offset = 0;
    ui->label_38->setText("Отступ " + QString::number(_offset) + " мкм");
    on_pbSetSteps_clicked();
}

void MainWindow::accurateToNull()
{
    QString stDefault = "background:rgb(0,255,0);";
    qreal accuracy = 0.005f;
    while (!compare(_encoderPort->toMmFromNull(1), 0, accuracy)) {
        if (_forceStop)
            break;
        if (_encoderPort->toMmFromNull(1) > 0)
            emit moveDrive(2,0,1);
        else
            emit moveDrive(2,1,1);
        wait(200);
    }
    ui->lbD1Mm->setStyleSheet(stDefault);
    while (!compare(_encoderPort->toMmFromNull(2), 0, accuracy)) {
        if (_forceStop)
            break;
        if (_encoderPort->toMmFromNull(2) > 0)
            emit moveDrive(4,1,1);
        else
            emit moveDrive(4,0,1);
        wait(200);
    }
    ui->lbD2Mm->setStyleSheet(stDefault);
    while (!compare(_encoderPort->toMmFromNull(3), 0, accuracy)) {
        if (_forceStop)
            break;
        if (_encoderPort->toMmFromNull(3) > 0)
            emit moveDrive(6,1,1);
        else
            emit moveDrive(6,0,1);
        wait(200);
    }
    ui->lbD3Mm->setStyleSheet(stDefault);
    while (!compare(_encoderPort->toMmFromNull(4), 0, accuracy)) {
        if (_forceStop)
            break;
        if (_encoderPort->toMmFromNull(4) > 0)
            emit moveDrive(8,1,1);
        else
            emit moveDrive(8,0,1);
        wait(200);
    }
    ui->lbD4Mm->setStyleSheet(stDefault);
}

void MainWindow::on_chbPaintNewFile_clicked(bool checked)
{
    _settings.setValue("PaintNewFile",checked);
}

void MainWindow::on_BtnReturn_clicked()
{
    on_pbToNull_clicked(true);
}

void MainWindow::on_pbToNull_clicked(bool checked)
{
    if (checked) {
        on_pbToNull3_clicked();
        returnToNull();
        waitAns(0);
        accurateToNull();
        ui->pbToNull->setChecked(false);
    } else {
        _forceStop = true;
    }
}
