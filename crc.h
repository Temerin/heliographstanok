#ifndef CRC_H_INCLUDED
#define CRC_H_INCLUDED


#include <stdint.h>
#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif

uint16_t CRC_8795(uint8_t *buf, unsigned len);
uint16_t CRC_16(uint8_t *buf, unsigned len);

#ifdef __cplusplus
}
#endif


#endif // CRC_H_INCLUDED
