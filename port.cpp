#include "port.h"
#include <qdebug.h>

Port::Port(QObject *parent) :
    QObject(parent)
{
}

Port::~Port()
{
    qDebug("By in Thread!");
    emit finished_Port();
}

void Port :: processPort(){
    qDebug("Hello World in Thread!");
    connect(&thisPort,SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
    connect(&thisPort, &QSerialPort::readyRead,this,&Port::ReadInPort);
}

void Port :: writeSettingsPort(QString name, int baudrate,int DataBits,
                         int Parity,int StopBits, int FlowControl){
    SettingsPort.name = name;
    SettingsPort.baudRate = (QSerialPort::BaudRate) baudrate;
    SettingsPort.dataBits = (QSerialPort::DataBits) DataBits;
    SettingsPort.parity = (QSerialPort::Parity) Parity;
    SettingsPort.stopBits = (QSerialPort::StopBits) StopBits;
    SettingsPort.flowControl = (QSerialPort::FlowControl) FlowControl;
}

void Port :: connectPort(void){//
    thisPort.setPortName(SettingsPort.name);
    if (thisPort.open(QIODevice::ReadWrite)) {
        if (thisPort.setBaudRate(SettingsPort.baudRate)
                && thisPort.setDataBits(SettingsPort.dataBits)//DataBits
                && thisPort.setParity(SettingsPort.parity)
                && thisPort.setStopBits(SettingsPort.stopBits)
                && thisPort.setFlowControl(SettingsPort.flowControl))
        {
            if (thisPort.isOpen()){
                error_((SettingsPort.name + tr(" >> Открыт!\r")));
                emit connected(true);
            }
        } else {
            thisPort.close();
            error_((SettingsPort.name + " >> " + thisPort.errorString()).toLocal8Bit());
            emit connected(false);
          }
    } else {
        thisPort.close();
        error_((SettingsPort.name + " >> " + thisPort.errorString()).toLocal8Bit());
        emit connected(false);
    }
}

void Port::handleError(QSerialPort::SerialPortError error)//
{
    if ( (thisPort.isOpen()) && (error == QSerialPort::ResourceError)) {
        error_((SettingsPort.name + " >> " + thisPort.errorString()).toLocal8Bit());
        disconnectPort();
    }
}


void  Port::disconnectPort(){
    if(thisPort.isOpen()){
        thisPort.close();
        error_(SettingsPort.name.toLocal8Bit() + " >> Закрыт!\r");
        emit connected(false);
    }
}

void Port::WriteToPort(QByteArray data) {
    if(thisPort.isOpen()) {
//        qDebug() << thisPort.portName() << ": " << data.toHex();
        thisPort.write(data);
        inPort(data);
    }
}
//
void Port::ReadInPort(){
    QByteArray data;
    data.append(thisPort.readAll());
    outPort(data);
}

