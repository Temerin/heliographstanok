#ifndef TOOTHTENZWRITER_H
#define TOOTHTENZWRITER_H

#include "logwriter.h"
#include <QMap>


class ToothTenzWriter : public LogWriter
{
public:
    explicit ToothTenzWriter(QObject *parent = nullptr);

    void newFile();

    void writeLog(QPoint xy, QMap<int, int> toothTenz);

private:
    void makeFile();
};

#endif // TOOTHTENZWRITER_H
