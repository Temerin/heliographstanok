#ifndef ROLEPORT_H
#define ROLEPORT_H

#include <QObject>
#include "port.h"





enum Alarm {
    noError                                         = 0x00,
    overCurrentDuringAcceleration,
    overCurrentDuringDeceleration,
    overCurrentDuringConstantSpeedOperation,
    overCurrentInLoadAtSturtup,
    shortCirultInArm,
    inputPhaseFailure,
    outputPhaseFailure,
    overvoltageDuringAcceleration,
    overvoltageDuringDeceleration,
    overvoltageDuringConstantSpeedOperation,
    overLoadInInverter,
    overLoadInMotor,
    overheatTrip = 0x10,
    emergencyStop,
    eepromFault1,
    eepromFault2,
    eepromFault3,
    ramFault,
    romFault,
    cpuFault,
    communicationErrorTrip,
    currentDetectorFault = 0x1A,
    optionalCircuitBoardTypeError,
    graphicKeypadCommunicationError,
    smallCurrentTrip,
    tripDueToUndervoltageInMainCircuit,
    overTorqueTrip                                  = 0x20,
    groundFaultTrip                                 = 0x22,
    overcurrentFlowingInElementDuringAcceleration   = 0x25,
    overcurrentFlowingInElementDuringDeceleration,
    overcurrentFlowingInElementDuringOperation,
    inverterTypeError                               = 0x29,
    externalThermalInput                            = 0x2E,
    viaCableBreak,
    breakInAnAnalogSignalCable                      = 0x32,
    cpuFault1                                       = 0x33,
    excessTorqueBoost                               = 0x34,
    cpuFault2                                       = 0x35,
    autoTuningError                                 = 0x54,
    closedDamper1Fault                              = 0x48,
    closedDamper2Fault                              = 0x49,

};



class QTimer;

class RolePort : public Port
{
public:
    RolePort();

    quint8 _readMode = 1;
    uint16_t _prevState = 0;
    uint16_t _prevFreq = 0;
    uint16_t _prevCurrent = 0;
    uint16_t _prevAlarm = 0;

signals:
    void state(uint16_t val);
    void freq(uint16_t val);
    void current(uint16_t val);
    void alarm(uint16_t val);

public slots:
    void move(int step=1, int speed=1, qint8 mode=1);
    void stop();
    void clear();

    void WriteToPort(QByteArray data);

    void ReadInPort();
    void GetError();
private slots:

    void pol();

private:

    QTimer *_timer{nullptr};
};

#endif // ROLEPORT_H
